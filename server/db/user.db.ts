import { Map } from "mongodb";
import { Schema, Model } from "mongoose";
import {IUser} from "../src/model/interfaces/user.interface"
import { conn } from "./conn";

const userSchema: Schema = new Schema({
    name: String,
    email: {
        type: String,
        required: true,
        unique: true
    }, 
    password: {
        type: String,
        required: true
    },
    city: String,
    adress: String,
    postco: Number,
    phone: String,
    birthday: Date,
    cardnr: String,
    expdate: Date,
    cvc: String,
    cart: {
        type: Map,
        of: Number
    },
});

export const userModel: Model<IUser> = conn.model<IUser>("User", userSchema);