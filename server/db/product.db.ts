import { Map } from "mongodb";
import { Schema, Model } from "mongoose";
import { IProduct } from "../src/model/interfaces/product.interface";
import { conn } from "./conn";

const productSchema: Schema = new Schema({
    name: {
        type: String,
        unique: true
    },
    category: String,
    price: Number,
    quantity: Number,
    description: String,
    image: String
});

export const productModel: Model<IProduct> = conn.model<IProduct>("Product", productSchema);