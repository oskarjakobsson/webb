import { Map } from "mongodb";
import { Schema, Model } from "mongoose";
//import {User} from "../src/model/user.class"
import {IAdmin} from "../src/model/interfaces/Iadmin.interface"
import { conn } from "./conn";

const adminSchema: Schema = new Schema({

    username: {
        type: String,
        required: true,
        unique: true
    }, 
    password: {
        type: String,
        required: true
    },
});

export const adminModel: Model<IAdmin> = conn.model<IAdmin>("Admin", adminSchema);