import { Map } from "mongodb";
import { Schema, Model } from "mongoose";
import { Order } from "../src/model/classes/order.class";
import { IOrder } from "../src/model/interfaces/order.interface";
import { conn } from "./conn";

const orderSchema: Schema = new Schema({
    user: String,
    items: {
        type: Map,
        of: Number
    },
    totalPrice: Number,
    isDelivered: Boolean,
    city: String,
    adress: String,
    postco: String,
    phone: String,
    datePlaced: Date,
    
}, {timestamps: true}); 
export const orderModel: Model<IOrder> = conn.model<IOrder>("Order", orderSchema);