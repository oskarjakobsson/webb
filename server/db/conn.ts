import { create } from "domain";
import { MongoMemoryServer } from "mongodb-memory-server";
import { Connection, createConnection } from "mongoose";



let testConn: Connection;
let mongod: MongoMemoryServer;
let dbUrl: string = "mongodb+srv://admin:tasteful@cluster0.4z61h.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";


if (process.env.NODE_ENV === "test") {
    dbUrl = "mongodb+srv://admin:tasteful@cluster0.4z61h.mongodb.net/testDatabase?retryWrites=true&w=majority";
}


export const conn = createConnection(dbUrl);
