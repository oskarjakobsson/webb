import mongoose from "mongoose";

export interface IOrder extends mongoose.Document {
    user: string;
    items: Map<string, number>;
    totalPrice: Number;
    isDelivered: Boolean;
    city: string;
    adress: string;
    postco: string;
    phone: string;
    datePlaced: Date;
  }