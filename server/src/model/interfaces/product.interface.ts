import mongoose from "mongoose";
export interface IProduct extends mongoose.Document {
    name: string;
    category: string;
    price: number;
    quantity: number;
    description: string;
    image: string;
  }