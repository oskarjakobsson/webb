import mongoose from "mongoose";
export interface IAdmin extends mongoose.Document {
    username: string,
    password: string,
  }