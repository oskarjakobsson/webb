import mongoose from "mongoose";

export interface IUser extends mongoose.Document {
    name: string,
    email: string,
    password: string,
    city: string,
    adress: string,
    postco: number,
    phone: string,
    birthday: Date,
    cardnr?: string,
    expdate?: Date,
    cvc?: string,
    cart: Map<string, number>,
  }