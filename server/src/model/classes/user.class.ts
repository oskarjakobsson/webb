import mongoose from "mongoose";
import { IProduct } from "../interfaces/product.interface";
import { IUser } from "../interfaces/user.interface";
import axios, { AxiosResponse } from 'axios';
export class User {

  _userModel: IUser;

  constructor(_userModel: IUser) {
    this._userModel = _userModel;
  }

  //Legacy function. Not removing yet tho, might change my mind.
  async calculateTotalCost(): Promise<number> {
    
    let cost = 0;
    if (this._userModel.cart.size == 0) {
      return 0;
    }
    let products: IProduct [] = [];
  
    let productRes = await axios.get<Array<IProduct>>("http://localhost:8080/product/allProducts");
    products = productRes.data;
   
    this._userModel.cart.forEach((value: number, key: string) => {
      cost = cost + products.find(element => element.name == key)!.price * value;
    });

    return cost;
  }
}