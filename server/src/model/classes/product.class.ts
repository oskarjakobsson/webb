import mongoose from "mongoose";
import { IProduct } from "../interfaces/product.interface";
import { IUser } from "../interfaces/user.interface";
import axios, { AxiosResponse } from 'axios';
export class Product {

  _productModel: IProduct;

  constructor(_productModel: IProduct) {
    this._productModel = _productModel;
  }

}