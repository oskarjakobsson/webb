import mongoose from "mongoose";
import { IOrder } from "../interfaces/order.interface";

export class Order  {
    
    _orderModel: IOrder;

    constructor(_orderModel: IOrder){
        this._orderModel = _orderModel;
    }

    public checkDeliverStatus(): string {

        if(this._orderModel.isDelivered){
            return ("Delivered");
        }
        return ("In transit")
    }
}