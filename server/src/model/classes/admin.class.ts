import mongoose from "mongoose";
import { IProduct } from "../interfaces/product.interface";
import { IUser } from "../interfaces/user.interface";
import axios, { AxiosResponse } from 'axios';
import { IAdmin } from "../interfaces/Iadmin.interface";
export class Admin {

  _adminModel: IAdmin;

  constructor(_adminModel: IAdmin) {
    this._adminModel = _adminModel;
  }

}