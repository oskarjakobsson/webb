import { frontendUser } from "../../../../client/tasteful/src/model/frontendUser.class";
import { userDBService } from "../user.db.service";
import { cartDBService } from "../cart.db.service";
import { orderDBService } from "../order.db.service";
import { conn } from "../../../db/conn";

import { productDBService } from "../product.db.service";
import { IProduct } from "../../model/interfaces/product.interface";




const resetDB = async (collection: string) => {
    try {

        //Dont have permissions to drop entire db, can probably change but this is faster for now.

        //Btw dont worry this does nothing to the production dataBase, 
        //in conn there is an if-statement and if it is a test run there is a testDB that conn connects to
        await conn.dropCollection(collection);

    } catch (err) {
        console.log(err);
        process.exit(1);
    }
};

/**
 * 
 * 
 * Sorry for having such a large class! Couldnt figure out how to make more modular tests in time. 
 * I have tried to mark the tests clearly to make it easier to navigate.
 * 
 * 
 */

describe('ServiceLayer test', () => {
    beforeAll(() => {
    });


    afterAll(() => {
        conn.close();
    });


    // ================ tests for users ================================ //
    describe('User tests', () => {

        beforeEach(async () => {
            // Some tests wont actually create an input and since of async they wont always be run in sequence (I think?). 
            // It is 1 day until project deadline.
            // Solution? Create a new user before each test and then clear the collection => each test runs with a clean collection of users.
            let sendUser = new frontendUser("Oskar", "stupidIsAsStupidDoes@notlegit.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);
            await resetDB("users");

        });

        it('Create a user', async () => {
            //await resetDB("users");

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            expect(dbUser._userModel.name).toEqual(sendUser.name);
            expect(dbUser._userModel.email).toEqual(sendUser.email);
            expect(dbUser._userModel.password).toEqual(sendUser.password);
            expect(dbUser._userModel.city).toEqual(sendUser.city);
            expect(dbUser._userModel.adress).toEqual(sendUser.adress);
            expect(dbUser._userModel.postco).toEqual(sendUser.postco);
            expect(dbUser._userModel.phone).toEqual(sendUser.phone);
            expect(dbUser._userModel.birthday).toEqual(sendUser.birthday);

            //Cart is a different type of object in Database and typescript.
            //Have to check some properties of cart instead 
            expect(dbUser._userModel.cart.size).toEqual(sendUser.cart.size);
            expect(dbUser._userModel.cart.keys()).toEqual(sendUser.cart.keys());
            expect(dbUser._userModel.cart.values()).toEqual(sendUser.cart.values());

        });

        it('Create user with same email', async () => {


            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await expect(userDBService.addUserInfo(sendUser))
                .rejects
                .toThrow('Email already registered!');

        });

        it('Create a user without passsword', async () => {

            let sendUser = new frontendUser("Oskar", "nowitworks@email.com", "", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());

            await expect(userDBService.addUserInfo(sendUser))
                .rejects
                .toThrow("User validation failed: password: Path `password` is required.");


        });


        it('Add users paymentinfo', async () => {

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);         
            let res = await userDBService.addPaymentInfo(dbUser._userModel._id, "1251212", new Date("2023/02/12"), "123");

            expect(res._userModel.cardnr).toEqual("1251212");
            expect(res._userModel.expdate).toEqual(new Date("2023/02/12"));
            expect(res._userModel.cvc).toEqual("123");

        });

        it('Add invalid paymentinfo expired card', async () => {

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await expect(userDBService.addPaymentInfo(dbUser._userModel._id, "1251212", new Date("2022/01/15"), "123"))
                .rejects
                .toThrow("Expired card");

        });

        it('Add invalid paymentinfo no card number', async () => {

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await expect(userDBService.addPaymentInfo(dbUser._userModel._id, "", new Date("2025/01/15"), "123"))
                .rejects
                .toThrow("Invalid card number");

        });

        it('Add invalid paymentinfo no cvc', async () => {

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await expect(userDBService.addPaymentInfo(dbUser._userModel._id, "1251212", new Date("2025/01/15"), ""))
                .rejects
                .toThrow("Invalid cvc");
        });

        it('Check successful login', async () => {

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await userDBService.checkLogin(sendUser.email, sendUser.password);

            await expect(userDBService.checkLogin(sendUser.email, sendUser.password))
                .resolves
                .not
                .toThrow("invalid password or email");
        });

        it('Check nonSuccesful login', async () => {

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await expect(userDBService.checkLogin("wrongemail@emailserver.com", sendUser.password))
                .rejects
                .toThrow("invalid password or email");

            await expect(userDBService.checkLogin(sendUser.email, "hackerPassword"))
                .rejects
                .toThrow("invalid password or email");
        });

        it('Update user info correctly', async () => {

            let sendUser = new frontendUser("Oskar", "testemail@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            sendUser.name = "Cooler name";

            await userDBService.updateUserInfo(sendUser, dbUser._userModel.id);

            let updatedUser = await userDBService.getUser(dbUser._userModel.id);

            expect(updatedUser._userModel.name).toEqual("Cooler name");

        });

    });

    // ================ tests for products ================================ //
    describe('Products tests', () => {

        beforeEach(async () => {
            await resetDB("products");
        });

        it('Create a new product', async () => {

            let productObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 23,
                quantity: 410000,
                description: "Super tasty!",
                image: "imagestring"
            };

            let product = await productDBService.addProduct(productObject);

            expect(product.name).toEqual(productObject.name);
            expect(product.category).toEqual(productObject.category);
            expect(product.price).toEqual(productObject.price);
            expect(product.quantity).toEqual(productObject.quantity);
            expect(product.description).toEqual(productObject.description);
            expect(product.image).toEqual(productObject.image);

        });

        it('Get a product', async () => {

            let productObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 23,
                quantity: 410000,
                description: "Super tasty!",
                image: "imagestring"
            };

            let createdProduct = await productDBService.addProduct(productObject);
            let expectedProduct = await productDBService.getProduct(createdProduct.name);

            expect(createdProduct.name).toEqual(expectedProduct.name);
            expect(createdProduct.category).toEqual(expectedProduct.category);
            expect(createdProduct.price).toEqual(expectedProduct.price);
            expect(createdProduct.quantity).toEqual(expectedProduct.quantity);
            expect(createdProduct.description).toEqual(expectedProduct.description);
            expect(createdProduct.image).toEqual(expectedProduct.image);



        });

        it('Update existing product', async () => {

            let productObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 23,
                quantity: 410000,
                description: "Super tasty!",
                image: "imagestring"
            };

            let product = await productDBService.addProduct(productObject);

            product.quantity = 12;
            product.price = 2000;
            product.description = "Rare and expensive";

            await productDBService.updateProduct(product.name, product);

            let updatedProduct = await productDBService.getProduct(product.name);

            expect(updatedProduct.name).toEqual(product.name);
            expect(updatedProduct.category).toEqual(product.category);
            expect(updatedProduct.price).toEqual(product.price);
            expect(updatedProduct.quantity).toEqual(product.quantity);
            expect(updatedProduct.description).toEqual(product.description);
            expect(updatedProduct.image).toEqual(product.image);

        });

        it('Get products from an order', async () => {
            await resetDB("orders");


            //To get from products order we need to create an order, and to create an order we need a user and products.

            //Create productObjects
            let apricotObject: any = {
                name: "Apricot",
                category: "Yellow",
                price: 12,
                quantity: 410000,
                description: "Great!",
                image: "imagestring"
            };

            let strawberryObject: any = {
                name: "Strawberry",
                category: "red",
                price: 4,
                quantity: 12592,
                description: "Super tasty!",
                image: "imagestring"
            };

            let lemonObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 23,
                quantity: 78192,
                description: "Super tasty!",
                image: "imagestring"
            };


            //Add products to test DB
            await productDBService.addProduct(apricotObject);
            await productDBService.addProduct(strawberryObject);
            await productDBService.addProduct(lemonObject);


            let expectedList: IProduct[] = [];

            //push products into expectedList
            let product = await productDBService.getProduct("Apricot");
            expectedList.push(product);

            product = await productDBService.getProduct("Strawberry");
            expectedList.push(product);

            product = await productDBService.getProduct("Lemon");
            expectedList.push(product);

            //User that will place an order
            let sendUser = new frontendUser("Oskar", "newUser@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(),
                new Map<string, number>(), 0, "12345125", new Date("2025/01/12"), "103");
            let dbUser = await userDBService.addUserInfo(sendUser);


            //add the products to the cart
            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Strawberry", 2);
            await cartDBService.addToCart(dbUser._userModel.id, "Lemon", 5);


            //Place the order
            await orderDBService.placeOrder(dbUser._userModel.id, 159, new Date());

            //get the products from the order
            let resultList = await productDBService.getAllProductsFromOrder(dbUser._userModel.id);
            expect(resultList).toEqual(expectedList);

        });

        it('Get products from a cart', async () => {

            await resetDB("users");

            //To get from products a cart we need to create a cart, and to create a cart we need a user and products.

            //Create productObjects
            let apricotObject: any = {
                name: "Apricot",
                category: "Yellow",
                price: 12,
                quantity: 410000,
                description: "Great!",
                image: "imagestring"
            };

            let strawberryObject: any = {
                name: "Strawberry",
                category: "red",
                price: 4,
                quantity: 12592,
                description: "Super tasty!",
                image: "imagestring"
            };

            let lemonObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 23,
                quantity: 78192,
                description: "Super tasty!",
                image: "imagestring"
            };


            //Add products to test DB
            await productDBService.addProduct(apricotObject);
            await productDBService.addProduct(strawberryObject);
            await productDBService.addProduct(lemonObject);


            let expectedList: IProduct[] = [];

            //push products into expectedList
            //have to get them this way to ensure "toEqual" works on the entire list
            let product = await productDBService.getProduct("Apricot");
            expectedList.push(product);

            product = await productDBService.getProduct("Strawberry");
            expectedList.push(product);

            product = await productDBService.getProduct("Lemon");
            expectedList.push(product);

            //User that will add to its cart
            let sendUser = new frontendUser("Oskar", "newUser@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(),
                new Map<string, number>(), 0, "12345125", new Date("2025/01/12"), "103");
            let dbUser = await userDBService.addUserInfo(sendUser);


            //add the products to the cart
            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Strawberry", 2);
            await cartDBService.addToCart(dbUser._userModel.id, "Lemon", 5);

            let resultList = await productDBService.getProductsInCart(dbUser._userModel._id);
            expect(resultList).toEqual(expectedList);

        });

        it('Get all products', async () => {


            //Create productObjects
            let apricotObject: any = {
                name: "Apricot",
                category: "Yellow",
                price: 12,
                quantity: 410000,
                description: "Great!",
                image: "imagestring"
            };

            let strawberryObject: any = {
                name: "Strawberry",
                category: "red",
                price: 4,
                quantity: 12592,
                description: "Super tasty!",
                image: "imagestring"
            };

            let lemonObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 23,
                quantity: 78192,
                description: "Super tasty!",
                image: "imagestring"
            };

            let kiwiObject: any = {
                name: "Kiwi",
                category: "Green",
                price: 1245,
                quantity: 123441,
                description: "Good kiwi!",
                image: "imagestring"
            };


            //Add products to test DB
            await productDBService.addProduct(apricotObject);
            await productDBService.addProduct(strawberryObject);
            await productDBService.addProduct(lemonObject);
            await productDBService.addProduct(kiwiObject);


            let expectedList: IProduct[] = [];

            //push products into expectedList
            //have to get them this way to ensure "toEqual" works on the entire list
            let product = await productDBService.getProduct("Apricot");
            expectedList.push(product);

            product = await productDBService.getProduct("Strawberry");
            expectedList.push(product);

            product = await productDBService.getProduct("Lemon");
            expectedList.push(product);

            product = await productDBService.getProduct("Kiwi");
            expectedList.push(product);

            let resultList = await productDBService.getAllProducts();
            expect(resultList).toEqual(expectedList);

        });

    });


    // ================ tests for orders ================================ //
    describe('Order tests', () => {

        beforeEach(async () => {
            await resetDB("orders");
            await resetDB("users");
        });

        it('create a new order', async () => {

            let apricotObject: any = {
                name: "Apricot",
                category: "Yellow",
                price: 12,
                quantity: 410000,
                description: "Great!",
                image: "imagestring"
            };

            let strawberryObject: any = {
                name: "Strawberry",
                category: "red",
                price: 4,
                quantity: 12592,
                description: "Super tasty!",
                image: "imagestring"
            };
            let lemonObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 7,
                quantity: 1234124,
                description: "Sour",
                image: "imagestring"
            };



            //Add products to test DB
            await productDBService.addProduct(apricotObject);
            await productDBService.addProduct(strawberryObject);


            //User that will place an order
            let sendUser = new frontendUser("Oskar", "newUser@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(),
                new Map<string, number>(), 0, "12345125", new Date("2025/01/12"), "103");
            let dbUser = await userDBService.addUserInfo(sendUser);


            //add the products to the cart
            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Strawberry", 2);
            await cartDBService.addToCart(dbUser._userModel.id, "Lemon", 5);

            let expectedItems = new Map<string, number>();
            expectedItems.set("Apricot", 3);
            expectedItems.set("Strawberry", 2);
            expectedItems.set("Lemon", 5);


            //Place the order
            await orderDBService.placeOrder(dbUser._userModel.id, 159, new Date("2022/01/01"));

            //get the Order
            let resultOrder = await orderDBService.getOrders(dbUser._userModel._id);

            //local mockup order
            let expectedOrder: any = {
                user: dbUser._userModel.id,
                items: expectedItems,
                totalPrice: 159,
                isDelivered: false,
                city: dbUser._userModel.city,
                adress: dbUser._userModel.adress,
                postco: dbUser._userModel.postco,
                phone: dbUser._userModel.phone,
                datePlaced: new Date("2022/01/01"),
            }


            resultOrder[0]._orderModel.items.forEach((value: number, key: string) => {
                expect(expectedOrder.items.get(key)).toEqual(value);
            });

            expect(resultOrder[0]._orderModel.user).toEqual(expectedOrder.user);
            expect(resultOrder[0]._orderModel.totalPrice).toEqual(expectedOrder.totalPrice);
            expect(resultOrder[0]._orderModel.isDelivered).toEqual(expectedOrder.isDelivered);
            expect(resultOrder[0]._orderModel.city).toEqual(expectedOrder.city);
            expect(resultOrder[0]._orderModel.adress).toEqual(expectedOrder.adress);
            // Postco is Number at some places and string at other, cant change because using split at some places and to late to do the overall fix
            expect(parseInt(resultOrder[0]._orderModel.postco)).toEqual(expectedOrder.postco);
            expect(resultOrder[0]._orderModel.phone).toEqual(expectedOrder.phone);
            expect(resultOrder[0]._orderModel.datePlaced).toEqual(expectedOrder.datePlaced);

        });

        it('Set status to delivered', async () => {

            let apricotObject: any = {
                name: "Apricot",
                category: "Yellow",
                price: 12,
                quantity: 410000,
                description: "Great!",
                image: "imagestring"
            };

            //Add products to test DB
            await productDBService.addProduct(apricotObject);


            //User that will place an order
            let sendUser = new frontendUser("Oskar", "newUser@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(),
                new Map<string, number>(), 0, "12345125", new Date("2025/01/12"), "103");
            let dbUser = await userDBService.addUserInfo(sendUser);

            //add the products to the cart
            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);

            //Place the order
            let dbOrder = await orderDBService.placeOrder(dbUser._userModel.id, 159, new Date("2022/01/01"));

            expect(dbOrder._orderModel.isDelivered).toEqual(false);


            await orderDBService.setOrderToDelivered(dbOrder._orderModel._id);

            let dbOrders = await orderDBService.getOrders(dbUser._userModel.id);

            expect(dbOrders[0]._orderModel.isDelivered).toEqual(true);

        });

        it('Get all undelivered orders', async () => {

            let apricotObject: any = {
                name: "Apricot",
                category: "Yellow",
                price: 12,
                quantity: 410000,
                description: "Great!",
                image: "imagestring"
            };
            let strawberryObject: any = {
                name: "Strawberry",
                category: "red",
                price: 4,
                quantity: 12592,
                description: "Super tasty!",
                image: "imagestring"
            };
            let lemonObject: any = {
                name: "Lemon",
                category: "Yellow",
                price: 7,
                quantity: 1234124,
                description: "Sour",
                image: "imagestring"
            };

            //Add products to test DB
            await productDBService.addProduct(apricotObject);
            await productDBService.addProduct(strawberryObject);
            await productDBService.addProduct(lemonObject);


            //User that will place an order
            let sendUser = new frontendUser("Oskar", "newUser@emailserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(),
                new Map<string, number>(), 0, "12345125", new Date("2025/01/12"), "103");
            let dbUser = await userDBService.addUserInfo(sendUser);

            //add the products to the cart
            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await orderDBService.placeOrder(dbUser._userModel.id, 159, new Date("2022/01/01"));


            await cartDBService.addToCart(dbUser._userModel.id, "Lemon", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Strawberry", 6);
            await orderDBService.placeOrder(dbUser._userModel.id, 121, new Date("2022/01/01"));


            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Lemon", 3);
            let finalOrder = await orderDBService.placeOrder(dbUser._userModel.id, 121, new Date("2022/01/01"));


            //set one of three active orders to delivered
            await orderDBService.setOrderToDelivered(finalOrder._orderModel._id);

            //get all undeliveredorders, should be 2
            let listOforders = await orderDBService.getAllUndeliveredOrders();

            expect(listOforders.length).toEqual(2);
        });


    });

    // ================ tests for Cart ================================ //
    describe('Cart tests', () => {

        beforeEach(async () => {
            let sendUser = new frontendUser("Oskar", "notlegit@notlegit.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);
            await resetDB("users");
        });


        it('Add to cart and Get cart', async () => {
            let sendUser = new frontendUser("Oskar", "testuser@testserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Banana", 23);
            await cartDBService.addToCart(dbUser._userModel.id, "Fruity", 12);


            let cart = await cartDBService.getCart(dbUser._userModel._id);

            let expectedCart = new Map<string, number>();

            expectedCart.set("Apricot", 3);
            expectedCart.set("Banana", 23);
            expectedCart.set("Fruity", 12);

            cart.forEach((value: number, key: string) => {
                expect(expectedCart.get(key)).toEqual(value);
            });

        });

        it('Remove one item // reduce quantity by one', async () => {

            let sendUser = new frontendUser("Oskar", "testuser@testserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Banana", 23);
            await cartDBService.addToCart(dbUser._userModel.id, "Fruity", 12);

            await cartDBService.removeOneItem(dbUser._userModel.id, "Apricot");

            let expectedCart = new Map<string, number>();

            expectedCart.set("Apricot", 2);
            expectedCart.set("Banana", 23);
            expectedCart.set("Fruity", 12);

            let cart = await cartDBService.getCart(dbUser._userModel._id);

            cart.forEach((value: number, key: string) => {
                expect(expectedCart.get(key)).toEqual(value);
            });

        });

        it('Remove entire item', async () => {

            let sendUser = new frontendUser("Oskar", "testuser@testserver.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
            let dbUser = await userDBService.addUserInfo(sendUser);

            await cartDBService.addToCart(dbUser._userModel.id, "Apricot", 3);
            await cartDBService.addToCart(dbUser._userModel.id, "Banana", 23);
            await cartDBService.addToCart(dbUser._userModel.id, "Fruity", 12);

            await cartDBService.removeItem(dbUser._userModel.id, "Apricot");

            let expectedCart = new Map<string, number>();

            expectedCart.set("Banana", 23);
            expectedCart.set("Fruity", 12);

            let cart = await cartDBService.getCart(dbUser._userModel._id);

            cart.forEach((value: number, key: string) => {
                expect(expectedCart.get(key)).toEqual(value);
            });

        });

    });

});
