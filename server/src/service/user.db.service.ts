import { Console } from "console";
import { createCipheriv } from "crypto";
import { userModel } from "../../db/user.db";
import { User } from "../model/classes/user.class";
import { IUser } from "../model/interfaces/user.interface";




class UserDBService {
    //Hämtar alla users
    async getUsers(iddude: String): Promise<User[]> {

        return await userModel.find();

    }

    //Hämtar en user, den som har session
    async getUser(id: String): Promise<User> {
        const iuser = await userModel.findById(id);
        if (iuser == undefined) {
            throw new Error("no user");
        } else {
            return new User(iuser);
        }

    }
    async getCart(id: String) {
        const iuser = await userModel.findById(id);
        if (iuser == undefined) {
            throw new Error("no user");
        } else {
            return new User(iuser)._userModel.cart;
        }

    }


    async updateUserInfo(inputUser: any, id: string): Promise<User> {

        const iuser = await userModel.findById(id);
        if (iuser == undefined) {
            throw new Error("no user");
        }
        else {            
            await userModel.updateOne({ _id: id },
                {
                    $set: {
                        name: inputUser.name,
                        email: inputUser.email,
                        password: inputUser.password,
                        city: inputUser.city,
                        adress: inputUser.adress,
                        postco: inputUser.postco,
                        phone: inputUser.phone,
                        birthday: inputUser.birthday
                    }
                });
            return new User(iuser);

        }
    }



    async addUserInfo(inputUser: any): Promise<User> {


        let userCheck = await userModel.exists({ email: inputUser.email });

        if (userCheck != null) {
            throw new Error("Email already registered!");
        }
        let iuser = await userModel.create({
            name: inputUser.name,
            email: inputUser.email,
            password: inputUser.password,
            city: inputUser.city,
            adress: inputUser.adress,
            postco: inputUser.postco,
            phone: inputUser.phone,
            birthday: inputUser.birthday,
            cart: new Map<String, Number>(),
        })
        return new User(iuser);

    }

    async checkLogin(email: string, password: string): Promise<User> {

        let iuser = await userModel.findOne<IUser>({
            email: email,
            password: password
        });
        if (iuser == null || iuser == undefined) {
            throw new Error("invalid password or email");
        } else {

            return new User(iuser);
        }

    }


    async addPaymentInfo(id: String,
        cardnr: string,
        expdate: Date,
        cvc: string): Promise<User> {


    

        if (new Date(expdate) < new Date) {
            throw new Error("Expired card");
        }
        if(cardnr.length == 0){
            throw new Error("Invalid card number");
        }
        if(cvc.length != 3){
            throw new Error("Invalid cvc");
        }


        await userModel.updateOne({ _id: id },
            {
                $set: {
                    cardnr: cardnr,
                    expdate: expdate,
                    cvc: cvc
                }
            });
        const iuser = await userModel.findById(id);
        if (iuser == null) {
            throw new Error("no user");
        }
        else {
            return new User(iuser);
        }
    }
    async getTotalCost(id: string): Promise<number> {
        const iuser = await userModel.findById(id);
        if (iuser == null) {
            throw new Error("no user");
        }
        else {

            return new User(iuser).calculateTotalCost();
        }
    }

    //Måste gå att göra detta på ett smidigare sätt men just nu
    // Hämta usern => ändra på userobjektet lokalt => Skriv över objektet i databasen med updateone
    // Det är sent och inget annat har funkat.
    async addToCart(id: String,
        productname: string,
        quantity: number): Promise<User> {

        const iuser = await userModel.findById(id);

        if (iuser == null) {
            throw new Error("no user");
        }
        else {
            let user = new User(iuser);
            var oldq = user._userModel.cart.get(productname);
            if (oldq == undefined) {
                user._userModel.cart.set(productname, quantity);
            }
            else {
                user._userModel.cart.set(productname, quantity + oldq);
            }

            await userModel.updateOne({ _id: id },
                {
                    $set: {
                        cart: user._userModel.cart
                    }
                });
            return user;
        }
    }
}

export const userDBService = new UserDBService();