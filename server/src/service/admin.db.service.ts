import { Console } from "console";
import { createCipheriv } from "crypto";
import { adminModel } from "../../db/admin.db";
import { IAdmin } from "../model/interfaces/Iadmin.interface";




class AdminDBService {

    async getAdmin(id: String): Promise<IAdmin> {
        const admin = await adminModel.findById(id);
        if (admin == undefined) {
            throw new Error("undefiend admin");
        } else {
            return admin;
        }

    }
    async checkLogin(email: string, password: string): Promise<IAdmin> {
        const admin = await adminModel.findOne({
            email: email,
            password: password
        });
        if (admin == undefined) {
            throw new Error("invalid credentials");
        } else {
            return admin;
        }
    }

    async addAdmin(inputAdmin: IAdmin): Promise<IAdmin> {
        let adminCheck = await adminModel.exists({username: inputAdmin.username, password: inputAdmin.password});
        if(adminCheck != null){
            throw new Error("admin already registered!");
        }

        const admin = await adminModel.create({
            username: inputAdmin.username,
            password: inputAdmin.password,
        })
        if (admin == undefined) {
            throw new Error("undefined admin");
        } else {
            return admin;
        }

    }
}
export const adminDBService = new AdminDBService();