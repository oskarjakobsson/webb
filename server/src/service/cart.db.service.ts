import { Console } from "console";
import { createCipheriv } from "crypto";
import { userModel } from "../../db/user.db";
import { User } from "../model/classes/user.class";



class CartDBService {
    
    async getCart(id: String) {
        const iuser = await userModel.findById(id);
        if (iuser == undefined) {
            throw new Error("undefined user");
        } else {
            return iuser.cart;
        }

    }

    async removeOneItem(id: string, item: string): Promise<User> {
        const iuser = await userModel.findById(id);
        if (iuser == undefined) {
            throw new Error("undefined user");
        } else {
            let value = iuser.cart.get(item);
            if (value == undefined) {
                throw new Error("no val");
            }
            if (value > 1) {
                iuser.cart.set(item, value - 1);
            }
            else if (value <= 1) {
                iuser.cart.delete(item);
            }
            await userModel.updateOne({ _id: id },
                {
                    $set: {
                        cart: iuser.cart
                    }
                });
            return new User(iuser);

        }

    }
    async removeItem(id: string, item: string): Promise<User> {
        const iuser = await userModel.findById(id);
        if (iuser == undefined) {
            throw new Error("undefined user");
        } else {
            let value = iuser.cart.get(item);
            if (value == undefined) {
                throw new Error("no val");
            }
            iuser.cart.delete(item);
            await userModel.updateOne({ _id: id },
                {
                    $set: {
                        cart: iuser.cart
                    }
                });
            return new User(iuser);

        }

    }

    //Måste gå att göra detta på ett smidigare sätt men just nu
    // Hämta usern => ändra på userobjektet lokalt => Skriv över objektet i databasen med updateone
    // Det är sent och inget annat har funkat.
    async addToCart(id: String,
        productname: string,
        quantity: number): Promise<User> {

        const iuser = await userModel.findById(id);

        if (iuser == null) {           
            throw new Error("undefined user");
        }
        else {
            var oldq = iuser.cart.get(productname);
            if (oldq == undefined) {
                iuser.cart.set(productname, quantity);
            }
            else {
                iuser.cart.set(productname, quantity + oldq);
            }

            await userModel.updateOne({ _id: id },
                {
                    $set: {
                        cart: iuser.cart
                    }
                });
            return new User(iuser);
        }
    }

}
export const cartDBService = new CartDBService();