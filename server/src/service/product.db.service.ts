import { Console } from "console";
import { createCipheriv } from "crypto";
import { productModel } from "../../db/product.db";
import { userModel } from "../../db/user.db";
import { orderModel } from "../../db/order.db";
import { IProduct } from "../model/interfaces/product.interface";
import { frontendProduct } from "../../../client/tasteful/src/model/frontendProduct.class";
import { User } from "../model/classes/user.class";

class ProductDBService {

    async getAllProducts(): Promise<frontendProduct[]> {
        let productlist: IProduct[] = [];
        const products: IProduct[] = await productModel.find();
        if (products.length == 0) {
            throw new Error("No products");
        }

        products.forEach(product => {
            productlist.push(product);
        });

        return productlist;
    }

    async getProductsInCart(id: String): Promise<IProduct[]> {

        const iuser = await userModel.findById(id);

        let productlist: IProduct[] = [];
        if (iuser == null) {
            throw new Error("undefiend user");
        }
        let user = new User(iuser);
        for await (const val of user._userModel.cart) {
            let productid = await productModel.exists({ name: val[0] });
            if (productid != null) {
                const product = await productModel.findById(productid);
                productlist.push(product!);
            }
        }

        return productlist;

    }
    async getAllProductsFromOrder(id: String): Promise<IProduct[]> {

        const orders = await orderModel.find({ user: id });

        //user.cart.forEach
        let productlist: IProduct[] = [];
        if (orders == null) {
            throw new Error("no order!");
        }

        for await (const order of orders) {
            for await (const val of order.items) {
                let productid = await productModel.exists({ name: val[0] });
                if (productid != null) {
                    const product = await productModel.findById(productid);
                    productlist.push(product!);
                }
            }
        }
        return productlist;
    }

    async getProduct(name: String): Promise<IProduct> {

        const id = await productModel.exists({ name: name });
        const product = await productModel.findById(id);
        if (product == undefined) {
            throw new Error("No product");
        }
        return product;

    }
    async addProduct(inputProduct: IProduct): Promise<IProduct> {

        const product: IProduct = await productModel.create({
            name: inputProduct.name,
            category: inputProduct.category,
            price: inputProduct.price,
            quantity: inputProduct.quantity,
            description: inputProduct.description,
            image: inputProduct.image,
        })
        return product;
    }

    async updateProduct(oldName: string, inputProduct: IProduct): Promise<IProduct> {
        const updatedProduct = await productModel.updateOne({ name: oldName },
            {
                $set: {
                    name: inputProduct.name,
                    category: inputProduct.category,
                    price: inputProduct.price,
                    quantity: inputProduct.quantity,
                    description: inputProduct.description,
                    image: inputProduct.image,
                }
            });

        if (updatedProduct.matchedCount == 0) {
            throw new Error("No product with that name exists");
        }
        const product = await productModel.findOne({ name: inputProduct.name });        
        if (product == undefined) {
            throw new Error("No product was updated");
        }
        return product;
    }

    async getProductInCat(category: string): Promise<IProduct[]> {
        const product: Array<IProduct> = await productModel.find({ category: category });

        product.map(element => {
            console.log(element.name);
        })
        if (product == undefined) {
            throw new Error("No product")!
        }

        return product;

    }

}
export const productDBService = new ProductDBService();