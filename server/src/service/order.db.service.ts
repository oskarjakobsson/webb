import { Address } from "cluster";
import { Console } from "console";
import { createCipheriv } from "crypto";
import { userModel } from "../../db/user.db";
import { productModel } from "../../db/product.db";
import { orderModel } from "../../db/order.db";
import { Order } from "../model/classes/order.class";
import { IProduct } from "../model/interfaces/product.interface";
import { IOrder } from "../model/interfaces/order.interface";


class OrderDBService {


    //Return orders that match input id (user)
    async getOrders(id: String): Promise<Order[]> {
        let orderRes: IOrder[] = await orderModel.find({ user: id });
        let returnOrders: Order[] = [];
        orderRes.forEach((value) => {

            returnOrders.push(new Order(value));

        });
        return returnOrders;
    }
    //stupidly typed method because typescript is stupid with types
    async getDeliveredOrders(id: String): Promise<Order[]> {

        let orderRes: IOrder[] = (await orderModel.find({ user: id }));
        let deliveredOrders: Order[] = [];

        orderRes.forEach((value) => {
            if (value.isDelivered) {
                deliveredOrders.push(new Order(value));
            }
        });


        return deliveredOrders;
    }

    async getAllUndeliveredOrders(): Promise<Order[]> {
        let orderRes: IOrder[] = (await orderModel.find({ isDelivered: false }));
        let nondeliveredOrders: Order[] = [];

        //Isdelivered didnt work on orderRes because of how the class was designed or something? Something to do with extending mongoose document perhaps.
        //Couldnt call any functions or create new instances.
        //So now Order is the class and IOrder is the interface
        //you need an instance of IOrder to create a classobject of Order Hence the foreach loop
        
        orderRes.forEach((value) => {
            nondeliveredOrders.push(new Order(value));
        });

        return nondeliveredOrders;
    }

    async getUndeliveredOrders(id: String): Promise<Order[]> {


        let orderRes: IOrder[] = (await orderModel.find({ user: id }));
        let nondeliveredOrders: Order[] = [];

        //Isdelivered didnt work on orderRes because of how the class was designed or something? Something to do with extending mongoose document perhaps.
        //Couldnt call any functions or create new instances.
        //So now Order is the class and IOrder is the interface
        //you need an instance of IOrder to create a classobject of Order Hence the foreach loop
        orderRes.forEach((value) => {
            if (!value.isDelivered) {
                nondeliveredOrders.push(new Order(value));
            }
        });

        return nondeliveredOrders;
    }


    async placeOrder(id: String, totalPrice: Number, datePlaced: Date): Promise<Order> {

        const user = await userModel.findById(id);

        if (user == undefined) {
            throw new Error("undefined user");
        }
       

        const placedOrder: IOrder = await orderModel.create({
            user: id,
            items: user.cart,
            totalPrice: totalPrice,
            city: user.city,
            adress: user.adress,
            postco: user.postco,
            phone: user.phone,
            isDelivered: false,
            datePlaced: datePlaced,
        })
        if (placedOrder == undefined) {
            throw new Error("Failed to create order");
        }
        user.cart.clear();
        let test = await userModel.findByIdAndUpdate({ _id: id },
            {
                $set: {
                    cart: user.cart,
                }
            }
        );

        let returnOrder: Order = new Order(placedOrder);

        return (returnOrder);

    }

    async setOrderToDelivered(id: String): Promise<Order> {

        let orderRes = await orderModel.findOneAndUpdate({ _id: id }, { isDelivered: true });
        if (orderRes == undefined) {
            throw new Error("No such order");
        }
        let returnOrder: Order = new Order(orderRes);
        return returnOrder;
    }
}

export const orderDBService = new OrderDBService();