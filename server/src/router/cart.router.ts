import express, { Request, Response } from "express";
import { Order } from "../model/classes/order.class";
import { User } from "../model/classes/user.class";
import { userDBService } from "../service/user.db.service";
import { orderDBService } from "../service/order.db.service";
import { cartDBService } from "../service/cart.db.service";

/**
 * Router for all things related to a users cart.
 */
export const cartRouter = express.Router();

/**
 * Update the cart for a particular user
 */
cartRouter.put("/", async (req: Request, res: Response) => {
    try {
        if (req.session.userid == undefined) {
            res.status(400).send("Not logged in");
            return;
        }
        const user: User = await cartDBService.addToCart(req.session.userid, req.body.productname, req.body.quantity);
        res.status(200).send(user);
    } catch (e: any) {
        console.log('noooo');
        res.status(500).send(e.message);
    }
});

/**
 * Get the cart for a particular user
 */
cartRouter.get("/", async (req: Request, res: Response) => {
    try {

        if (req.session.userid == undefined) {
            res.status(400).send("Not logged in!");
            return;
        }

        const carts = await cartDBService.getCart(req.session.userid);

        res.status(200).send(carts);

    } catch (e: any) {
        res.status(500).send(e.message);

    }
});


/**
 * Removes one item (decreases the quantity by 1) from cart
 */
cartRouter.put("/removeoneitem", async (req: Request, res: Response) => {
    try {
        if (req.session.userid == undefined) {
            res.status(400).send("Missing user\n");
            return;
        }
        const item: string = req.body.item;
        const user: User = await cartDBService.removeOneItem(req.session.userid, item);
        res.status(200).send(user._userModel.cart);
        return;

    } catch (e: any) {
        console.log("i Failed!" + e.message);
        res.status(500).send(e.message);
    }
});

/**
 * Removes one item completely from cart
 */
cartRouter.put("/removeitem", async (req: Request, res: Response) => {
    try {
        if (req.session.userid == undefined) {
            res.status(400).send("Missing user\n");
            return;
        }
        const item: string = req.body.item;
        const user: User = await cartDBService.removeItem(req.session.userid, item);
        res.status(200).send(user._userModel.cart);
        return;
    } catch (e: any) {
        console.log("i Failed!" + e.message);
        res.status(500).send(e.message);

    }
});

/**
 * Adds exactly one item to a cart
 */
cartRouter.put("/additem", async (req: Request, res: Response) => {
    try {
        if (req.session.userid == undefined) {
            res.status(400).send("Missing user\n");
            return;
        }
        const item: string = req.body.item;
        const user: User = await cartDBService.addToCart(req.session.userid.toString(), item, 1);
        res.status(200).send(user._userModel.cart);
        return;
    } catch (e: any) {
        console.log("i Failed!" + e.message);
        res.status(500).send(e.message);

    }
});