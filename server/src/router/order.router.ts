import express, { Request, Response } from "express";
import { Order } from "../model/classes/order.class";
import { userDBService } from "../service/user.db.service";
import { frontendOrder } from "../../../client/tasteful/src/model/frontendOrder.class"
import { orderDBService } from "../service/order.db.service";


/**
 * Router for all things related to router
 */
export const orderRouter = express.Router();

/**
 * Gets all orders for a particular user
 */
orderRouter.get("/", async (req: Request, res: Response) => {
    try {
        if (req.session.userid != undefined) {
            const orders: Order[] = await orderDBService.getOrders(req.session.userid);
            const sendOrders: frontendOrder[] = [];
            for (const order of orders) {
                sendOrders.push(new frontendOrder(order._orderModel.user, order._orderModel._id, order._orderModel.items, order._orderModel.totalPrice,
                    order._orderModel.city, order._orderModel.adress, order._orderModel.postco, order._orderModel.phone,
                    order._orderModel.datePlaced, order.checkDeliverStatus()));
            }
            res.status(200).send(sendOrders);
        }
        else {
            return res.status(500).send("No user");;
        }
    } catch (e: any) {
        res.status(500).send(e.message);
    }
});

orderRouter.get("/allUndelivered", async (req: Request, res: Response) => {
    try {

        const orders: Order[] = await orderDBService.getAllUndeliveredOrders();
        const sendOrders: frontendOrder[] = [];
        for (const order of orders) {
            sendOrders.push(new frontendOrder(order._orderModel.user, order._orderModel._id, order._orderModel.items, order._orderModel.totalPrice,
                order._orderModel.city, order._orderModel.adress, order._orderModel.postco, order._orderModel.phone,
                order._orderModel.datePlaced, order.checkDeliverStatus()));
        }
        res.status(200).send(sendOrders);

    } catch (e: any) {
        res.status(500).send(e.message);
    }
});



/**
 * Place and order
 */
orderRouter.post("/", async (req: Request, res: Response) => {

    const totalPrice: number = req.body.totalPrice;
    const datePlaced: Date = new Date(req.body.datePlaced);

    try {
        if (req.session.userid != undefined) {
            const order: Order = await orderDBService.placeOrder(req.session.userid, totalPrice, datePlaced);
            const sendOrder: frontendOrder = new frontendOrder(order._orderModel.user, order._orderModel.user, order._orderModel.items, order._orderModel.totalPrice,
                order._orderModel.city, order._orderModel.adress, order._orderModel.postco, order._orderModel.phone,
                order._orderModel.datePlaced, order.checkDeliverStatus());
            res.status(200).send(sendOrder);
        }
        else {
            return res.status(500).send("No user");;
        }

    } catch (e: any) {

        res.status(500).send(e.message);

    }

});

/**
 * Change an order from undelivered to delivered
 */
orderRouter.put("/setToDelivered", async (req: Request, res: Response) => {

    try {
        const id = req.body.id;
        const order: Order = await orderDBService.setOrderToDelivered(id);
        res.status(200).send(order);

    } catch (e: any) {

        res.status(500).send(e.message);

    }
});