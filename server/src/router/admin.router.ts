import express, { Request, Response } from "express";
import { IAdmin } from "../model/interfaces/Iadmin.interface";
import { adminDBService } from "../service/admin.db.service";

/**
 * Router for updating user information and verifying session and log in
 */
export const adminRouter = express.Router();

adminRouter.get("/", async (req: Request, res: Response) => {
   try {

      if (req.session.adminid != undefined) {
         const admin: IAdmin = await adminDBService.getAdmin(req.session.adminid);
         res.status(200).send(admin);
      }
      else {
         return res.status(500).send("No admin");;
      }
   } catch (e: any) {
      res.status(500).send(e.message);
   }
});

adminRouter.post("/", async (req: Request, res: Response) => {
   try {
      const admin: IAdmin = await adminDBService.addAdmin(req.body.admin);
      req.session.adminid = admin._id;
      res.status(200).send(admin);
      return;

   } catch (e: any) {
      console.log("i Failed!" + e.message);
      res.status(500).send(e.message);
   }
});

adminRouter.get("/checkSession", async (req: Request, res: Response) => {
   try {
      if (req.session.adminid == undefined) {
         res.status(200).send("no session");
      } else {
         res.status(200).send("session exists")
      }
      return;
   } catch (e: any) {
      console.log(e.message);
      res.status(500).send(e.message);
   }
});

adminRouter.put("/login", async (req: Request, res: Response) => {
   try {
      const email: string = req.body.admin.email;
      const password: string = req.body.admin.password;

      const admin: IAdmin = await adminDBService.checkLogin(email, password);
      req.session.adminid = admin._id;

      res.status(200).send(admin);
      return;
   } catch (e: any) {
      res.status(500).send(e.message);
   }
});

/**
 * Kills the session when a user logs out
 */
adminRouter.get("/logout", async (req: Request, res: Response) => {
   try {
      req.session.adminid = undefined;
      if (req.session.userid == undefined) {
         req.session.destroy(function (err) {
            res.status(200).send("Session dead");
            return;
         })
         return;
      }
      res.status(200).send(req.session.adminid);
      return;
   } catch (e: any) {
      res.status(500).send(e.message);
   }
});