import express, { Request, Response } from "express";
import { IProduct } from "../model/interfaces/product.interface";
import { productDBService } from "../service/product.db.service";
import { frontendProduct } from "../../../client/tasteful/src/model/frontendProduct.class"
/**
 * Router for all things related to products
 */
export const productRouter = express.Router();


/**
 * Gets all products in the database
 */

productRouter.get("/allProducts", async (req: Request, res: Response) => {
   try {

      const products: Array<frontendProduct> = await productDBService.getAllProducts();
      res.status(200).send(products);
      return products;

   } catch (e: any) {

      res.status(500).send(e.message);

   }

});

/**
 * Gets the products currently in the cart for the current user.
 */
productRouter.get("/usersProduct", async (req: Request, res: Response) => {
   try {
      if (req.session.userid == undefined) {
         throw new Error("no userSesh!");
      }
      const products: Array<IProduct> = await productDBService.getProductsInCart(req.session.userid);
      res.status(200).send(products);
      return products;

   } catch (e: any) {

      res.status(500).send(e.message);

   }

});
/**
 * Gets the ordered products for the current user.
 */
productRouter.get("/orderedproducts", async (req: Request, res: Response) => {
   try {
      // Finns express tool för att visa map-data
      //const name = req.body.data.name;
      if (req.session.userid == undefined) {
         throw new Error("no userSesh!");
      }
      const products: Array<IProduct> = await productDBService.getAllProductsFromOrder(req.session.userid);
      res.status(200).send(products);
      return products;

   } catch (e: any) {

      res.status(500).send(e.message);

   }

});


productRouter.get("/:category", async (req: Request, res: Response) => {
   try {
      const products: Array<IProduct> = await productDBService.getProductInCat(req.params.category);
      res.status(200).send(products);
      return products;

   } catch (e: any) {

      res.status(500).send(e.message);

   }

});

/**
 * Create a new product(currently hardcoded in service layer).
 */
productRouter.post("/", async (req: Request, res: Response) => {

   try {

      const product: IProduct = await productDBService.addProduct(req.body.product);
      console.log(product);
      res.status(200).send(product);

   } catch (e: any) {

      res.status(500).send(e.message);

   }

});

productRouter.put("/updateProduct", async (req: Request, res: Response) => {

   try {

      const product: IProduct = await productDBService.updateProduct(req.body.oldName, req.body.product);
      res.status(200).send(product);


   } catch (e: any) {

      res.status(500).send(e.message);

   }

});