import express, { Request, Response } from "express";
import { User } from "../model/classes/user.class";
import { userDBService } from "../service/user.db.service";
import { frontendUser } from "../../../client/tasteful/src/model/frontendUser.class"

/**
 * Router for updating user information and verifying session and log in
 */
export const userRouter = express.Router();

/**
 * Gets the currently logged in user. If no user is logged in returns 500 with message "No user"
 */
userRouter.get("/", async (req: Request, res: Response) => {
   try {

      if (req.session.userid != undefined) {
         const user: User = await userDBService.getUser(req.session.userid);
         const sendUser: frontendUser = new frontendUser(user._userModel.name, user._userModel.email, user._userModel.password, 
                                                         user._userModel.city, user._userModel.adress, user._userModel.postco, 
                                                         user._userModel.phone, user._userModel.birthday, user._userModel.cart, 
                                                         await user.calculateTotalCost(), user._userModel.cardnr, user._userModel.expdate, user._userModel.cvc);
         res.status(200).send(sendUser);
      }
      else {
         return res.status(500).send("No user");;
      }
   } catch (e: any) {
      res.status(500).send(e.message);
   }
});

/**
 * Gets all users, never used this one probably no use? Maybe for testcases
 */
userRouter.get("/allusers", async (req: Request, res: Response) => {
   try {
      if (req.session.userid != undefined) {
         const users: Array<User> = await userDBService.getUsers(req.session.userid);
         res.status(200).send(users);
      }
      else {
         return;
      }
   } catch (e: any) {
      res.status(500).send(e.message);
   }
});

/**
 * Kills the session when a user logs out
 */
userRouter.get("/logout", async (req: Request, res: Response) => {
   try {
      req.session.userid = undefined;
      if(req.session.adminid == undefined){
         req.session.destroy(function (err) {
            res.status(200).send("Session dead");
            return;
         })
         return;
      }
      res.status(200).send(req.session.userid);
      return;

   } catch (e: any) {
      res.status(500).send(e.message);
   }
});

/**
 * Check if there is an active session
 */
userRouter.get("/checkSession", async (req: Request, res: Response) => {
   try {
      if (req.session.userid == undefined) {
         res.status(200).send("no session");
      } else {
         res.status(200).send("session exists")
      }
      return;
   } catch (e: any) {
      console.log(e.message);
      res.status(500).send(e.message);
   }
});


/**
 * Checks if user credentials exist in database
 */
userRouter.put("/login", async (req: Request, res: Response) => {
   try {
      const email: string = req.body.user.email;
      const password: string = req.body.user.password;

      const user: User = await userDBService.checkLogin(email, password);
      req.session.userid = user._userModel._id; 
    
      const sendUser: frontendUser = new frontendUser(user._userModel.name, user._userModel.email, user._userModel.password, 
         user._userModel.city, user._userModel.adress, user._userModel.postco, 
         user._userModel.phone, user._userModel.birthday, user._userModel.cart, 
         await user.calculateTotalCost(), user._userModel.cardnr, user._userModel.expdate, user._userModel.cvc);


      res.status(200).send(sendUser);
      
      return;
   } catch (e: any) {
      res.status(500).send(e.message);
   }
});

/**
 * Creates a new user
 */
userRouter.post("/", async (req: Request, res: Response) => {
   try {
      
      const user: User = await userDBService.addUserInfo(req.body.user);
      req.session.userid = user._userModel._id;
      

      const sendUser: frontendUser = new frontendUser(user._userModel.name, user._userModel.email, user._userModel.password, 
         user._userModel.city, user._userModel.adress, user._userModel.postco, 
         user._userModel.phone, user._userModel.birthday, user._userModel.cart, 
         await user.calculateTotalCost(), user._userModel.cardnr, user._userModel.expdate, user._userModel.cvc);
         res.status(200).send(sendUser);
      
         return;
   } catch (e: any) {
      console.log("i Failed!" + e.message);
      res.status(500).send(e.message);
   }
});

/**
 * Updates already existing user
 */
userRouter.put("/", async (req: Request, res: Response) => {
   try {

      if (req.session.userid == undefined) {
         res.status(400).send("Missing user\n");
         return;
      }

      const user: User = await userDBService.updateUserInfo(req.body.user, req.session.userid);
      const sendUser: frontendUser = new frontendUser(user._userModel.name, user._userModel.email, user._userModel.password, 
         user._userModel.city, user._userModel.adress, user._userModel.postco, 
         user._userModel.phone, user._userModel.birthday, user._userModel.cart, 
         await user.calculateTotalCost(), user._userModel.cardnr, user._userModel.expdate, user._userModel.cvc);
      
         res.status(200).send(sendUser);
      return;

   } catch (e: any) {
      console.log("i Failed!");
      res.status(500).send(e.message);
   }

});

/**
 * Updates and adds paymentinfo to already existing user
 */
userRouter.put("/payment", async (req: Request, res: Response) => {
   try {
      if (req.session.userid == undefined) {
         res.status(400).send("Missing user\n");
         return;
      }
      const cardnr: string = req.body.user.cardnr;
      const expdate: Date = new Date(req.body.user.expdate);
      const cvc: string = req.body.user.cvc;

      const user: User = await userDBService.addPaymentInfo(req.session.userid, cardnr, expdate, cvc);
      const sendUser: frontendUser = new frontendUser(user._userModel.name, user._userModel.email, user._userModel.password, 
         user._userModel.city, user._userModel.adress, user._userModel.postco, 
         user._userModel.phone, user._userModel.birthday, user._userModel.cart, 
         await user.calculateTotalCost(), user._userModel.cardnr, user._userModel.expdate, user._userModel.cvc);
         
      res.status(200).send(sendUser);

   } catch (e: any) {
      res.status(500).send(e.message);
   }
});
