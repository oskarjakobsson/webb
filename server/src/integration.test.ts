import supertest, * as Supertest from "supertest";
import { app } from "./start";
import { frontendUser } from "../../client/tasteful/src/model/frontendUser.class";
import { conn } from "../db/conn";
import { IProduct } from "./model/interfaces/product.interface";
import { userDBService } from "./service/user.db.service";
var session = require('supertest-session');

//axios.defaults.withCredentials = true;


var testSession = null;
const request: Supertest.SuperTest<Supertest.Test> = Supertest.default(app);

describe('Integration test', () => {
   beforeAll(() => {

   });


   afterAll(() => {
   });

   describe('User tests', () => {
      beforeEach(async () => {


      });
      it('Create a user', async () => {

         testSession = session(app);
         let sendUser = new frontendUser("Oskar", "yeyeyeyeyye@notlegit.com", "secretpassword", "cool city", "cool street 12", 4925, "07244483123", new Date(), new Map<string, number>());
         const resUser = await request.post('/user').send({ user: sendUser });

         expect(resUser.body.name).toEqual(sendUser.name);
         expect(resUser.body.email).toEqual(sendUser.email);
         expect(resUser.body.password).toEqual(sendUser.password);
         expect(resUser.body.city).toEqual(sendUser.city);
         expect(resUser.body.adress).toEqual(sendUser.adress);
         expect(resUser.body.postco).toEqual(sendUser.postco);
         expect(resUser.body.phone).toEqual(sendUser.phone);
         expect(new Date(resUser.body.birthday)).toEqual(new Date(sendUser.birthday));
         
      });

   });
});

