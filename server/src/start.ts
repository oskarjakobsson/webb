/**

 * Required External Modules

 */



import express from "express";
import session from "express-session"
import cors from "cors";
import * as path from "path";

import { userRouter } from "./router/user.router";
import { productRouter } from "./router/product.router";
import { orderRouter } from "./router/order.router";
import { cartRouter } from "./router/cart.router";
import { adminRouter } from "./router/admin.router";


/**
 
 * App Variables
 
 */


export const app = express();



/**
 
 * App Configuration
 
 */


declare module "express-session" {
  interface SessionData {
    userid: string,
    adminid: string,
  }
}

var sess = {
  userid: "",
  secret: "secret",
  cookie: {
    maxAge: 24 * 60 * 60 * 1000,
    secure: false
  },
  resave: false,
  saveUninitialized: true
};
app.use(express.json());
app.use(cors({
  origin: true,
  credentials: true,
}));

app.use(session(sess));

app.use("/user", userRouter);
app.use("/product", productRouter);
app.use("/order", orderRouter);
app.use("/cart", cartRouter);
app.use("/admin", adminRouter);


app.use(express.static(path.join(__dirname, '../../client/tasteful/build')));


app.get('/*', (req: express.Request, res: express.Response) => {
  res.sendFile(path.join(__dirname, '../../client/tasteful/build', 'index.html'));
});

