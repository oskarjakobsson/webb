import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import HomePage from './views/user/HomePage';
import PaymentPage from './views/user/PaymentPage';
import ProfilePage from './views/user/ProfilePage';
import GetCategory from './views/user/Product';
import CartPage from './views/user/CartPage';
import AdminAddProduct from './views/admin/AdminAddProduct';
import AdminLogin from './views/admin/AdminLogin';
import AdminPage from './views/admin/AdminPage';
import AdminRegister from './views/admin/AdminRegister';
import AdminUpdateProduct from './views/admin/AdminUpdateProduct';
import LoginPage from './views/user/LoginPage';
import OrderPage from './views/user/OrderPage';
import RegisterPage from './views/user/RegisterPage';
import AdminManageOrders from './views/admin/AdminManageOrders';
import AdminManageProfiles from './views/admin/AdminManageProfiles';


ReactDOM.render(

  <BrowserRouter>
  <Routes>
      <Route path="/" element={<HomePage />}> </Route>
        <Route path="PaymentPage" element={<PaymentPage />} />
        <Route path="ProfilePage" element={<ProfilePage />} />
        <Route path="ProductPage/:category" element={<GetCategory/>}  />
        <Route path="CartPage" element={<CartPage />} />
        <Route path="LoginPage" element={<LoginPage />} />
        <Route path="RegisterPage" element={<RegisterPage />} />
        <Route path="OrderPage" element={<OrderPage />} />
        <Route path="AdminRegister" element={<AdminRegister />} />
        <Route path="AdminLogin" element={<AdminLogin />} />
        <Route path="AdminPage" element={<AdminPage />} />
        <Route path="AdminUpdateProduct" element={<AdminUpdateProduct />} />
        <Route path="AdminAddProduct" element={<AdminAddProduct />} />
        <Route path="AdminManageOrders" element={<AdminManageOrders />} />
        <Route path="AdminManageProfiles" element={<AdminManageProfiles />} />
        
      
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
