import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card, Accordion, Badge } from 'react-bootstrap';
import Layout from '../../components/layout';
import { Link, Navigate } from 'react-router-dom';
import { Admin } from "../../model/admin.class";
import { frontendOrder } from "../../model/frontendOrder.class";
import { IProduct } from '../../../../../server/src/model/interfaces/product.interface';
import NonLoggedInAdmin from '../../components/NonLoggedInAdmin';
import loginChecker from '../../model/loginCheck';
import LoadingCard from '../../components/LoadingCard';
import AdminLayout from '../../components/adminLayout';
axios.defaults.withCredentials = true;


class AdminManageOrders extends React.Component<{}, { loadedPage: boolean, admin: Admin, orders: frontendOrder[], products: IProduct[], loggedIn: boolean }> {

    constructor(props: {}) { // Använd user istället för state
        super(props);

        this.state = {
            loadedPage: false,
            admin: new Admin(
                "",
                "",
            ),
            orders: [],
            products: [],
            loggedIn: false,
        };

        this.componentDidMount = async () => {

            let loginCheck = new loginChecker();
            if (await loginCheck.check("admin")) {

                let resAdmin = await axios.get("http://localhost:8080/admin");
                const allOrderRes = await axios.get("http://localhost:8080/order/allUndelivered");
                let orders: frontendOrder[] = (allOrderRes.data as frontendOrder[]);
                let productRes = await axios.get<Array<IProduct>>("http://localhost:8080/product/allProducts");
                orders = orders.reverse();
               
                this.setState({
                    admin: new Admin(resAdmin.data.username, resAdmin.data.password),
                    loggedIn: true,
                    loadedPage: true,
                    orders: orders,
                    products: productRes.data,
                });

            }
            else {
                this.setState({
                    loggedIn: false,
                    loadedPage: true
                });
            }

        }

    }

    createChildren(value: number, key: string) {
        let items: any = [];
        let index = 0;
        items.push(<td key= {index} className='orderTd'>{key}</td>);
        items.push(<td key= {index+1} className='orderTd'>{value} st</td>);
        items.push(<td key= {index+2} className='orderTd'> {this.state.products.find(element => element.name == key)!.price * value} kr</td>);
        return items;
    }
    createTable(children: any[]) {
        let table: any = [];
        table.push(
            <table key = {0} className="orderTable">
                <thead>
                    <tr>
                        <th className='orderTh'>Item</th>
                        <th className='orderTh'>Quantity</th>
                        <th className='orderTh'>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {children}
                </tbody>
            </table>
        );
        return table;
    }
    createCard(value: frontendOrder, table: any[]) {
        let card: any = [];
        card.push(
            <Card key = {0} bsPrefix='productCard'>
                <Card.Body>
                    <Card.Title>OrderId: {value.orderID} </Card.Title>
                    <Card.Text>Status: {value.delieredString}</Card.Text>
                    <Card.Text>Ordered on : {new Date(value.datePlaced).toString().substring(0, 21)}</Card.Text>
                    <Card.Text>Ordered by: {value.user}</Card.Text>
                    <Card.Text>Order adress: {value.adress + ", " + value.postco.slice(0, 3) + " " + value.postco.slice(3, 5) + ", " + value.city}</Card.Text>
                    <Card.Text>totalCost: {value.totalPrice} kr</Card.Text>
                    <Card.Text bsPrefix="orderHeadline">Ordered items</Card.Text>
                    {table}
                    <br></br>
                    <Button onClick={() => this.changeStatus(value.orderID)}> Change to delivered </Button>
                </Card.Body>
            </Card>);
        return card;
    }

    createOrderView() {

        let returnList: any = [];
        let outerIndex = 0;
        this.state.orders.forEach((value) => {
            let items: any = [];
            let table: any = [];
            let children: any = [];
            let localMap = new Map<string, number>();
            let innerIndex = 0;
            localMap = new Map(Object.entries(value.items));
            localMap.forEach((value: number, key: string) => {
                items = this.createChildren(value, key);
                children.push(<tr key = {innerIndex}>{items}</tr>);
                innerIndex++;
            });

            table = this.createTable(children);

            let card: any = [];
            card = this.createCard(value, table);
            returnList.push(
                <Row key = {outerIndex} className=' flex mb-4'>
                    <Col className='d-flex justify-content-center'>
                        {card}                        
                    </Col>
                </Row>
            );

        });
        return returnList;
    }

    async changeStatus(id: string){
        //change the status of the chosen order then reload the window
        await axios.put("http://localhost:8080/order/setToDelivered", { id: id });
        window.location.reload();
    }



    render() {
        if (!this.state.loadedPage) {
            return (
                <AdminLayout>
                    <Container>
                        <LoadingCard></LoadingCard>
                    </Container>
                </AdminLayout>
            );
        }
        if (!this.state.loggedIn) {
            return (<NonLoggedInAdmin />)
        }
        if (this.state.orders.length == 0) {
            return (
                <AdminLayout>
                    <Container className="h-100">
                        <Row className=' flex mb-4'>
                            <Col className='d-flex justify-content-center'>
                                <Card bsPrefix='noProdCard'>
                                    <Card.Body>
                                        <Card.Title>No Orders</Card.Title>
                                        <Card.Text>There are no undelivered orders</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </AdminLayout>
            );
        }
        return (
            <AdminLayout>
                <Container>                   
                    {this.createOrderView()}
                </Container>
            </AdminLayout>
        );
    }
};

export default AdminManageOrders;