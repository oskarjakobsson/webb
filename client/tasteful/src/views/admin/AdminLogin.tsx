import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import { Link, Navigate } from 'react-router-dom';
import { Admin } from "../../model/admin.class"
import AdminLayout from '../../components/adminLayout';
axios.defaults.withCredentials = true;

class AdminLogin extends React.Component<{}, { admin: Admin, redirect: boolean }> {

  constructor(props: {}) { // Använd user istället för state
    super(props);

    this.state = {
      admin: new Admin(
        "",
        "",
      ),
      redirect: false,
    };

    this.componentDidMount = async () => {
      let success: boolean = true;
      const res = await axios.get("http://localhost:8080/admin/checkSession");
      console.log(res.data);
      if(res.data == "no session"){
        return;
      }
      
      this.setState({ redirect: true });

    }

  }

  /**
   * onLogin checks if the inputted information is correct and in the case of an unsuccessful attempt it alerts the user that the input was incorrect
   * On a successful attempt redirects the user to their profilepage
   * @returns sets the state depending on the outcome of login attempt
   */
  private async onLogin() {
    let success: boolean = true;
    await axios.put<never>("http://localhost:8080/admin/login", { admin: this.state.admin }).catch(err => {
      if (err.response) {
        alert("Invalid password or email!");
        success = false;
      }
    });

    if (!success) return;
      this.setState({ redirect: true });

  }


  render() {
    if (this.state.redirect) {
      return <Navigate to="/AdminPage" />;
    }
    return (
      <AdminLayout>
        <Container >
          <Row className="justify-content-md-center">
            <Col xs lg="6">
              <Card>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicUsernam">
                      <Form.Label className='small mb-1'>Username</Form.Label>
                      <Form.Control type="text" placeholder="Enter Username" value={this.state.admin.username} onChange={e => this.setState(prevState => ({
                        admin: {                   // object that we want to update
                          ...prevState.admin,    // keep all other key-value pairs
                          username: e.target.value      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label className='small mb-1'>Password</Form.Label>
                      <Form.Control type="password" placeholder="Enter password" value={this.state.admin.password} onChange={e => this.setState(prevState => ({
                        admin: {                   // object that we want to update
                          ...prevState.admin,    // keep all other key-value pairs
                          password: e.target.value.toString()      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Button variant="primary" onClick={() => this.onLogin()} >
                      Login
                    </Button>

                  </Form> <br></br>
                  Dont have an account? <Link to="/AdminRegister">Request admin account here! </Link>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </AdminLayout>
    );
  }
};

export default AdminLogin;