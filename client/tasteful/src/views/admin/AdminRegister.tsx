import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import { Navigate } from 'react-router-dom';
import {Admin} from "../../model/admin.class"
import AdminLayout from '../../components/adminLayout';
axios.defaults.withCredentials = true;

class AdminRegister extends React.Component<{}, {admin: Admin, redirect: boolean }> {

    constructor(props: {}) { // Använd user istället för state
      super(props);
  
      this.state = {
        admin: new Admin(
            "",
          "",
        ),
        redirect: false,
      };
  
      this.componentDidMount = async () => {
        
      }
      this.onAddAdmin = this.onAddAdmin.bind(this);
    }

    

    private async onAddAdmin() {
        let sucessfulRegister: boolean = true;
        await axios.post<never>("http://localhost:8080/admin", { admin: this.state.admin }).catch(function (error) {
          if (error.response) {
            alert(error.response.data);
            console.log(error.response.data);
            sucessfulRegister = false;
          }
        });
        if (sucessfulRegister) {
          this.setState({
            redirect: true,
          });
        }
      }


  render() {
    if (this.state.redirect) {
      return <Navigate to="/AdminPage" />;
    }
    return (
      <AdminLayout>
        <Container >
          <Row className="justify-content-md-center">
            <Col xs lg="6">
              <Card>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicUsername">
                      <Form.Label className='small mb-1'>Admin username</Form.Label>
                      <Form.Control type="text" placeholder="Enter admin Username" value={this.state.admin.username} onChange={e => {
                      this.setState(prevState => ({
                        admin: {                   // object that we want to update
                          ...prevState.admin,    // keep all other key-value pairs
                          username: e.target.value      // update the value of specific key
                        }
                      })); console.log(this.state.admin); }} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label className='small mb-1'>Password</Form.Label>
                      <Form.Control type="password" placeholder="Enter password" value={this.state.admin.password} onChange={e =>  {
                        this.setState(prevState => ({
                        admin: {                   // object that we want to update
                          ...prevState.admin,    // keep all other key-value pairs
                          password: e.target.value.toString()      // update the value of specific key
                        }
                      })); console.log(this.state.admin); }} />
                    </Form.Group>
                    <Button variant="primary" onClick={this.onAddAdmin} >
                      Register
                    </Button>
                  </Form>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </AdminLayout>
    );
  }
};

export default AdminRegister;