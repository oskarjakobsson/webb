import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';

import { Link, Navigate } from 'react-router-dom';
import { Admin } from "../../model/admin.class"
import { frontendProduct } from "../../model/frontendProduct.class"
import {loginChecker} from "../../model/loginCheck"
import AdminLayout from '../../components/adminLayout'
import NonLoggedInAdmin from '../../components/NonLoggedInAdmin';
import LoadingCard from '../../components/LoadingCard';
axios.defaults.withCredentials = true;


class AdminAddProduct extends React.Component<{}, { product: frontendProduct, admin: Admin, loggedIn: boolean, loadedPage: boolean }> {

    constructor(props: {}) { // Använd user istället för state
        super(props);

        this.state = {
            admin: new Admin(
                "",
                "",
            ),
            product: new frontendProduct("", "", 0, 0, "", ""),
            loggedIn: false,
            loadedPage: false,
        };

        this.componentDidMount = async () => {
            let loginCheck = new loginChecker();
            if(await loginCheck.check("admin")){
                let resAdmin = await axios.get("http://localhost:8080/admin");
                this.setState({
                    admin: new Admin(resAdmin.data.username, resAdmin.data.password),
                    loggedIn: true,
                    loadedPage: true
                });
            }
            else{
                this.setState({
                    loggedIn: false,
                    loadedPage: true
                });
            }
        }
    }

    async addProduct() {
        let uglyBoolean: boolean = false;
        await axios.post<never>("http://localhost:8080/product", { product: this.state.product }).catch(function (error) {
            if (error.response) {
                alert(error.response.data);
                uglyBoolean = true;
            }
        });
        if (uglyBoolean) {
            return;
        }
        alert("Product added!");
        window.location.reload();
    }
    addImage(inputFile: File) {

        let localFile = inputFile;
        const acceptedImageTypes = ['image/jpeg', 'image/png', 'image/jpg'];
        if(!acceptedImageTypes.includes(localFile.type)){
            alert("Only the image types jpeg, jpg and png are allowed!");
            window.location.reload();
            return;
        }

        var reader = new FileReader();
        reader.readAsDataURL(localFile);
        reader.onload = (e) => {
            if (reader.result == null) {
                return;
            }
            this.setState(prevState => ({
                product: {                   // object that we want to update
                    ...prevState.product,    // keep all other key-value pairs
                    image: (reader.result as string)   // update the value of specific key
                }
            }))
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
        return;
    }

    render() {
        if (!this.state.loadedPage) {
            return (
                <AdminLayout>
                    <Container>
                        <LoadingCard></LoadingCard>
                    </Container>
                </AdminLayout>
            );
        }
        if(!this.state.loggedIn){
            return(<NonLoggedInAdmin/>);
        }
        return (
            <AdminLayout>
                <Container>
                    <h1 className="adminHeader">Add new product</h1>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Body>
                                    <Form>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicName">
                                                    <Form.Label className='small mb-1'>Name</Form.Label>
                                                    <Form.Control type="text" placeholder="Enter productname" value={this.state.product.name} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            name: e.target.value      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicCategory">
                                                    <Form.Label className='small mb-1'>Category</Form.Label>
                                                    <Form.Control type="text" placeholder="Enter category" value={this.state.product.category} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            category: e.target.value      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicPrice">
                                                    <Form.Label className='small mb-1'>Price</Form.Label>
                                                    <Form.Control type="tel" placeholder="Enter price" value={this.state.product.price.toString()} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            price: parseInt(e.target.value)      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicQuantity">
                                                    <Form.Label className='small mb-1'>Quantity</Form.Label>
                                                    <Form.Control type="tel" placeholder="Enter Quantity" value={this.state.product.quantity.toString()} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            quantity: parseInt(e.target.value)       // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicDescription">
                                                    <Form.Label className='small mb-1'>Description</Form.Label>
                                                    <Form.Control type="text" placeholder="Enter description" value={this.state.product.description} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            description: e.target.value      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicFile">
                                                    <Form.Label className='small mb-1'>Upload File</Form.Label>
                                                    <Form.Control type="file" onChange={(e: any) => {
                                                        this.addImage(e.target.files[0]);
                                                    }
                                                    } />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Button variant="primary" onClick={() => this.addProduct()} >
                                            Add product!
                                        </Button>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </AdminLayout>
        );
    }
};

export default AdminAddProduct;