import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import { Link, Navigate } from 'react-router-dom';
import { Admin } from "../../model/admin.class"
import AdminLayout from '../../components/adminLayout';
import NonLoggedInAdmin from '../../components/NonLoggedInAdmin';
import loginChecker from '../../model/loginCheck';
import LoadingCard from '../../components/LoadingCard';
axios.defaults.withCredentials = true;


class AdminManageProfiles extends React.Component<{}, { admin: Admin, loggedIn: boolean, loadedPage: boolean }> {

    constructor(props: {}) { // Använd user istället för state
        super(props);

        this.state = {
            admin: new Admin(
                "",
                "",
            ),
            loggedIn: false,
            loadedPage: false
        };

        this.componentDidMount = async () => {

            let loginCheck = new loginChecker();
            if (await loginCheck.check("admin")) {
                let resAdmin = await axios.get("http://localhost:8080/admin");
                this.setState({
                    admin: new Admin(resAdmin.data.username, resAdmin.data.password),
                    loggedIn: true,
                    loadedPage: true
                });
            }
            else {
                this.setState({
                    loggedIn: false,
                    loadedPage: true
                });
            }

        }
        this.onlogout = this.onlogout.bind(this);
    }


    private async onlogout() {
        await axios.get<never>("http://localhost:8080/admin/logout").then(() => {
            this.setState({ loggedIn: false });
        });

    }

    render() {
        if (!this.state.loadedPage) {
            return (
                <AdminLayout>
                    <Container>
                        <LoadingCard></LoadingCard>
                    </Container>
                </AdminLayout>
            );
        }
        if (!this.state.loggedIn) {
            return (<NonLoggedInAdmin />);
        }
        return (
            <AdminLayout>
                <Container>

                    <Row>
                        <Col>
                            <Card>
                                <Card.Body>
                                    <Form>

                                        <Form.Group className="mb-3" controlId="formBasicUsername">
                                            <Form.Label className='small mb-1'>Email address</Form.Label>
                                            <Form.Control type="text" placeholder="Enter username" value={this.state.admin.username} onChange={e => this.setState(prevState => ({
                                                admin: {                   // object that we want to update
                                                    ...prevState.admin,    // keep all other key-value pairs
                                                    username: e.target.value      // update the value of specific key
                                                }
                                            }))} />
                                        </Form.Group>


                                        <Form.Group className="mb-3" controlId="formBasicPassword">
                                            <Form.Label className='small mb-1'>Password</Form.Label>
                                            <Form.Control type="tel" placeholder="Enter password" value={this.state.admin.password} onChange={e => this.setState(prevState => ({
                                                admin: {                   // object that we want to update
                                                    ...prevState.admin,    // keep all other key-value pairs
                                                    password: e.target.value      // update the value of specific key
                                                }
                                            }))} />
                                        </Form.Group>


                                        <Button variant="primary" onClick={() => { console.log("hej"); }} >
                                            Submit
                                        </Button>
                                        <Button className="mx-3" variant="primary" onClick={this.onlogout} >
                                            Log Out
                                        </Button>

                                    </Form>
                                </Card.Body> </Card>

                        </Col>
                    </Row>
                </Container>
            </AdminLayout>
        );
    }
};

export default AdminManageProfiles;