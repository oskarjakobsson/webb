import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import { Link, Navigate } from 'react-router-dom';
import { Admin } from "../../model/admin.class"
import { frontendProduct } from "../../model/frontendProduct.class"
import AdminLayout from '../../components/adminLayout';
import { IProduct } from '../../../../../server/src/model/interfaces/product.interface';
import NonLoggedInAdmin from '../../components/NonLoggedInAdmin';
import loginChecker from '../../model/loginCheck';
import LoadingCard from '../../components/LoadingCard';
axios.defaults.withCredentials = true;


class AdminUpdateProduct extends React.Component<{}, { admin: Admin, product: frontendProduct, products: IProduct[], loggedIn: boolean, loadedPage: boolean, oldName: string }> {

    constructor(props: {}) { // Använd user istället för state
        super(props);

        this.state = {
            admin: new Admin(
                "",
                "",
            ),
            products: [],
            product: new frontendProduct("", "", 0, 0, "", ""),
            loggedIn: false,
            loadedPage: false,
            oldName: ""
        };

        this.componentDidMount = async () => {

            let loginCheck = new loginChecker();
            if (await loginCheck.check("admin")) {
                let resAdmin = await axios.get("http://localhost:8080/admin");
                const res: AxiosResponse<IProduct[]> = await axios.get<IProduct[]>("http://localhost:8080/product/allProducts");
                this.setState({
                    admin: new Admin(resAdmin.data.username, resAdmin.data.password),
                    loggedIn: true,
                    products: res.data,
                    loadedPage: true,
                    oldName: res.data[0].name,
                });

            }
            else {
                this.setState({
                    loggedIn: false,
                    loadedPage: true,
                });
            }
        }
    }

    async updateProduct() {

        let uglyBoolean: boolean = false;
        await axios.put<never>("http://localhost:8080/product/updateProduct", { oldName: this.state.oldName, product: this.state.product }).catch(function (error) {
            if (error.response) {
                uglyBoolean = true;
                alert(error.response.data);
                return;
            }
        });
        if (uglyBoolean) {
            return;
        }
        alert("Product updated!");
        window.location.reload();

    }
    generateOptions() {

        let index = 0;
        let options: any[] = [];
        this.state.products.map(product => {
            options.push(<option key={index}>{product.name}</option>);
            index++;
        });

        return options;
    }

    addImage(inputFile: File) {

        let localFile = inputFile;
        const acceptedImageTypes = ['image/jpeg', 'image/png', 'image/jpg'];
        if (!acceptedImageTypes.includes(localFile.type)) {
            alert("Only the image types jpeg, jpg and png are allowed!");
            window.location.reload();
            return;
        }

        var reader = new FileReader();
        reader.readAsDataURL(localFile);
        reader.onload = (e) => {
            if (reader.result == null) {
                return;
            }
            this.setState(prevState => ({
                product: {                   // object that we want to update
                    ...prevState.product,    // keep all other key-value pairs
                    image: (reader.result as string)   // update the value of specific key
                }
            }))
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
        return;
    }

    render() {
        if (!this.state.loadedPage) {
            return (
                <AdminLayout>
                    <Container>
                        <LoadingCard></LoadingCard>
                    </Container>
                </AdminLayout>
            );
        }
        if (!this.state.loggedIn) {
            return (<NonLoggedInAdmin />);
        }
        return (
            <AdminLayout>
                <h1 className="adminHeader">Update a product</h1>
                <Container>
                    <Row>
                        <Col>
                            <Card>
                                <Card.Body>
                                    <Form>

                                        <Form.Group controlId="formBasicSelect">
                                            <Form.Label>Select fruit</Form.Label>

                                            <Form.Select onChange={e => {
                                                console.log("e.target.value " + e.target.value);
                                                this.setState({
                                                    oldName: e.target.value,
                                                });
                                            }}>
                                                {this.generateOptions()}
                                            </Form.Select>
                                        </Form.Group>

                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicName">
                                                    <Form.Label className='small mb-1'>Name</Form.Label>
                                                    <Form.Control type="text" placeholder="Enter productname" value={this.state.product.name} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            name: e.target.value      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicCategory">
                                                    <Form.Label className='small mb-1'>Category</Form.Label>
                                                    <Form.Control type="text" placeholder="Enter category" value={this.state.product.category} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            category: e.target.value      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicPrice">
                                                    <Form.Label className='small mb-1'>Price</Form.Label>
                                                    <Form.Control type="tel" placeholder="Enter price" value={this.state.product.price.toString()} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            price: parseInt(e.target.value)      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicQuantity">
                                                    <Form.Label className='small mb-1'>Quantity</Form.Label>
                                                    <Form.Control type="tel" placeholder="Enter Quantity" value={this.state.product.quantity.toString()} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            quantity: parseInt(e.target.value)       // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicDescription">
                                                    <Form.Label className='small mb-1'>Description</Form.Label>
                                                    <Form.Control type="text" placeholder="Enter description" value={this.state.product.description} onChange={e => this.setState(prevState => ({
                                                        product: {                   // object that we want to update
                                                            ...prevState.product,    // keep all other key-value pairs
                                                            description: e.target.value      // update the value of specific key
                                                        }
                                                    }))} />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicFile">
                                                    <Form.Label className='small mb-1'>Upload File</Form.Label>
                                                    <Form.Control type="file" onChange={(e: any) => {
                                                        this.addImage(e.target.files[0]);
                                                    }
                                                    } />
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Button variant="primary" onClick={() => this.updateProduct()} >
                                            Update!
                                        </Button>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </AdminLayout>
        );
    }
};

export default AdminUpdateProduct;