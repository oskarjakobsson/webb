import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import lemon from '../../assets/lemon.png';
import { frontendUser } from "../../model/frontendUser.class";
import { Link, Outlet } from 'react-router-dom';

class PaymentPage extends React.Component<{}, { user: frontendUser }> {

  constructor(props: {}) { // Använd user istället för state
    super(props);

    this.state = {

      user: new frontendUser("",
        "",
        "",
        "",
        "",
        0,
        "",
        new Date(0),
        new Map<string, number>(),
        0,
        "",
        new Date(0),
        ""
      ),
    };

    this.updateInfo = this.updateInfo.bind(this);

    this.componentDidMount = async () => {
      const res = await axios.get<frontendUser>("http://localhost:8080/user");
      if (res.data.expdate != undefined && res.data.cvc != undefined && res.data.cardnr != undefined) {
        this.setState({
          user: res.data
        });
      }
    }
  }

  confirmExpdate(input: string) {
    this.setState(prevState => ({
      user: {                   // object that we want to update
        ...prevState.user,    // keep all other key-value pairs
        expdate: new Date(input),     // update the value of specific key
      }
    }));
  }

  confirmCardNumber(input: string) {
    if (input.length == 0) {
      this.setState(prevState => ({
        user: {                   // object that we want to update
          ...prevState.user,    // keep all other key-value pairs
          cardnr: ""      // update the value of specific key
        }
      }))
    }
    for (var i = 0; i < input.length; i++) {
      if (isNaN(parseInt(input[i]))) {
        return;
      }
    }
    this.setState(prevState => ({
      user: {                   // object that we want to update
        ...prevState.user,    // keep all other key-value pairs
        cardnr: input      // update the value of specific key
      }
    }))
  }

  confirmCVC(input: string) {
    if (input.length == 0) {
      this.setState(prevState => ({
        user: {                   // object that we want to update
          ...prevState.user,    // keep all other key-value pairs
          cvc: ""      // update the value of specific key
        }
      }))
    }
    for (var i = 0; i < input.length; i++) {
      if (isNaN(parseInt(input[i]))) {
        return;
      }
    }
    this.setState(prevState => ({
      user: {                   // object that we want to update
        ...prevState.user,    // keep all other key-value pairs
        cvc: input      // update the value of specific key
      }
    }))
  }

  isValidCard(): boolean {
    if (this.state.user.expdate == undefined) {
      alert("Please add the expiration date to your card!");
      return false;
    }
    if (this.state.user.cvc == "" || this.state.user.cvc!.length != 3) {
      alert("Please add a valid CVC to your credit card (3 digits)");
      return false;
    }
    if (this.state.user.cardnr == "") {
      alert("Please enter the card number of your credit card");
      return false;
    }
    if (new Date(this.state.user.expdate).getTime() < new Date().getTime()) {
      alert("Your card has expired!")
      return false;
    }
    return true;
  }

  private async updateInfo() {

    if (!this.isValidCard()) {
      return;
    }
    axios.put<never>("http://localhost:8080/user/payment", { user: this.state.user })
    window.location.reload();

  }

  checkDate() {
    if (this.state.user.expdate == undefined) {
      this.setState(prevState => ({
        user: {                   // object that we want to update
          ...prevState.user,    // keep all other key-value pairs
          expdate: new Date('December 17, 2000 03:24:00'),     // update the value of specific key
        }
      }));
    }
    else {
      return new Date(this.state.user.expdate).toISOString().split('T')[0];
    }
    return ""
  }

  render() {
    return (
      <Layout>
        <Container >
          <Navbar>
            <Nav className="me-auto">
              <Link to="/ProfilePage"> <p> Profile </p> </Link>
              <Link to="/PaymentPage"> <p> Payment </p> </Link>
              <Link to="/OrderPage"> <p> Orders </p> </Link>
            </Nav>
          </Navbar>
          <Row  >
            <Col md={3} lg={4} >
              <Card className='flex'> <Card.Img className=" img-account-profile rounded-circle mb-2 p-3" width={100} variant="top" src={lemon} />
                <Card.Body className='d-flex justify-content-center'>
                  <Button variant="primary" type="submit" >
                    Generate new fruit
                  </Button>
                </Card.Body> </Card>
            </Col>
            <Col>
              <Card>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicCardnr">
                      <Form.Label className='small mb-1'>CardNumber</Form.Label>
                      <Form.Control type="text" placeholder="Enter cardnr" value={this.state.user.cardnr} onChange={e => {
                        this.confirmCardNumber(e.target.value)
                      }} />
                    </Form.Group>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicExpdate">
                          <Form.Label className='small mb-1'>Expiry date</Form.Label>
                          <Form.Control type="date" value={this.checkDate()} onChange={e => {
                            this.confirmExpdate(e.target.value);
                          }} />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicCVC">
                          <Form.Label className='small mb-1'>CVC</Form.Label>
                          <Form.Control type="text" placeholder="Enter your CVC" value={this.state.user.cvc} onChange={e => {
                            this.confirmCVC(e.target.value)
                          }} />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Button variant="primary" onClick={this.updateInfo} >
                      Submit
                    </Button>
                  </Form>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </Layout>
    );
  }
};

export default PaymentPage;