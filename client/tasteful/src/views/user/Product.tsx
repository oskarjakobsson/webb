import React from 'react';
import { useParams } from 'react-router-dom';
import ProductPage from './ProductPage';

function GetCategory() {

    const { category } = useParams();

    return (
        <div>
            <ProductPage category={category!} />
        </div>
    );
}

export default GetCategory;