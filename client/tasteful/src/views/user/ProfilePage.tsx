import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import lemon from '../../assets/lemon.png';
import { Link, Navigate } from 'react-router-dom';

import { frontendUser } from "../../model/frontendUser.class";


class ProfilePage extends React.Component<{}, { user: frontendUser, redirect: Boolean }> {

  constructor(props: {}) {

    super(props);

    this.state = {

      user: new frontendUser("",
        "",
        "",
        "",
        "",
        0,
        "",
        new Date(0),
        new Map<string, number>(),
        0,
        "",
        new Date(0),
        ""
      ),
      redirect: false,
    };


    this.onUpdateUser = this.onUpdateUser.bind(this);
    this.logout = this.logout.bind(this);
    // Component did mount för att skapa user

    this.componentDidMount = async () => {
      // Ifall en användare har konto
      const res: AxiosResponse<frontendUser> = await axios.get<frontendUser>("http://localhost:8080/user");
      this.setState({
        user: new frontendUser(res.data.name,
          res.data.email,
          res.data.password,
          res.data.city,
          res.data.adress,
          res.data.postco,
          res.data.phone,
          new Date(res.data.birthday),
          res.data.cart,
          res.data.totalCost,
          res.data.cardnr,
          res.data.expdate,
          res.data.cvc,
        )
      });
    }
  }


  private async onUpdateUser() {

    if (this.state.user.email == "" || this.state.user.email == undefined) {
      alert("Invalid email");
      window.location.reload();
      return;
    }
    axios.put<never>("http://localhost:8080/user", { user: this.state.user })
    window.location.reload();

  }

  private async logout() {
    await axios.get<never>("http://localhost:8080/user/logout");
    this.setState({ redirect: true });
  }

  confirmNumber(input: string) {
    if (input.length == 0) {
      this.setState(prevState => ({
        user: {                   // object that we want to update
          ...prevState.user,    // keep all other key-value pairs
          phone: ""      // update the value of specific key
        }
      }))
    }

    for (var i = 1; i < input.length; i++) {
      if (isNaN(parseInt(input[i]))) {
        return;
      }
    }
    if (isNaN(parseInt(input[0])) && input[0] != "+") {
      return;
    }
    this.setState(prevState => ({
      user: {                   // object that we want to update
        ...prevState.user,    // keep all other key-value pairs
        phone: input      // update the value of specific key
      }
    }))
  }




  render() {
    if (this.state.redirect) {
      return <Navigate to="/Loginpage" />;
    }
    return (
      <Layout>
        <Container >
          <Navbar>
            <Nav className="me-auto">
              <Link to="/ProfilePage"> <p> Profile</p> </Link>
              <Link to="/PaymentPage"> <p> Payment</p> </Link>
              <Link to="/OrderPage"> <p> My orders</p> </Link>
            </Nav>
          </Navbar>
          <Row  >
            <Col md={3} lg={4} >
              <Card className='flex'> <Card.Img className=" img-account-profile rounded-circle mb-2 p-3" width={100} variant="top" src={lemon} />
                <Card.Body className='d-flex justify-content-center'>
                  <Button variant="primary" type="submit" >
                    Generate new fruit
                  </Button>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicName">
                      <Form.Label className='small mb-1'>Name</Form.Label>
                      <Form.Control type="text" placeholder="Enter name" value={this.state.user.name} onChange={e => this.setState(prevState => ({
                        user: {                   // object that we want to update
                          ...prevState.user,    // keep all other key-value pairs
                          name: e.target.value      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label className='small mb-1'>Email address</Form.Label>
                      <Form.Control type="email" placeholder="Enter email" value={this.state.user.email} onChange={e => this.setState(prevState => ({
                        user: {                   // object that we want to update
                          ...prevState.user,    // keep all other key-value pairs
                          email: e.target.value      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCity">
                      <Form.Label className='small mb-1'>City</Form.Label>
                      <Form.Control type="city" placeholder="Enter City" value={this.state.user.city} onChange={e => this.setState(prevState => ({
                        user: {                   // object that we want to update
                          ...prevState.user,    // keep all other key-value pairs
                          city: e.target.value      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicAdress">
                          <Form.Label className='small mb-1'>Adress</Form.Label>
                          <Form.Control type="address" placeholder="Enter adress" value={this.state.user.adress} onChange={e => this.setState(prevState => ({
                            user: {                   // object that we want to update
                              ...prevState.user,    // keep all other key-value pairs
                              adress: e.target.value      // update the value of specific key
                            }
                          }))} />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicPostalcode">
                          <Form.Label className='small mb-1'>Postal CO</Form.Label>
                          <Form.Control type="tel" placeholder="Enter postco" value={this.state.user.postco}
                            onChange={e => {
                              if (isNaN(parseInt(e.target.value))) {
                                e.target.value = "0";
                              }
                              this.setState(prevState => ({
                                user: {                   // object that we want to update
                                  ...prevState.user,    // keep all other key-value pairs
                                  postco: parseInt(e.target.value)      // update the value of specific key
                                }
                              }))
                            }
                            } />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicPhone">
                          <Form.Label className='small mb-1'>Phone</Form.Label>
                          <Form.Control type="tel" placeholder="Enter phonenumber" value={this.state.user.phone} onChange={e => {
                            //Dumt sätt för att kolla att inga siffror är med
                            //Behövdes för att kunna skriva nollor och för att slipa NaN
                            this.confirmNumber(e.target.value);
                          }} />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBirthday">
                          <Form.Label className='small mb-1'> Birthday</Form.Label>
                          <Form.Control type="date" value={this.state.user.birthday.toISOString().split('T')[0]} onChange={e =>
                            this.setState(prevState => ({
                              user: {                   // object that we want to update
                                ...prevState.user,    // keep all other key-value pairs
                                birthday: new Date(e.target.value)      // update the value of specific key
                              }
                            }))} />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Button variant="primary" onClick={this.onUpdateUser} >
                      Submit
                    </Button>
                    <Button className="mx-3" variant="primary" onClick={this.logout} >
                      Log Out
                    </Button>
                  </Form>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </Layout>
    );
  }
};

export default ProfilePage;