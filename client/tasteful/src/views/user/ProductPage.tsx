import React from 'react';
import '../../App.css'
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import lemon from '../../assets/lemon.png';
import banana from '../../assets/banana-3.png';
import pineapple from '../../assets/pineapple.png';
import Layout from '../../components/layout';
import { frontendUser } from "../../model/frontendUser.class";
import axios, { AxiosResponse } from 'axios';
import { IProduct } from '../../../../../server/src/model/interfaces/product.interface';


class ProductPage extends React.Component<{ category: string }, { user: frontendUser, products: IProduct[] }> {
  constructor(props: { category: string }) { // Använd user istället för state
    super(props);

    this.state = {
      user: new frontendUser("",
        "",
        "",
        "",
        "",
        0,
        "",
        new Date(0),
        new Map<string, number>(),
        0,
        "",
        new Date(0),
        ""
      ),
      products: [],
    };
    this.componentDidMount = async () => {
      const res: AxiosResponse<IProduct[]> = await axios.get<IProduct[]>("http://localhost:8080/product/" + this.props.category);
      this.setState({
        products: res.data,
      })
    }
  }

  onAddToCart(item: string) {
    axios.put<never>("http://localhost:8080/cart", { productname: item, quantity: 1 })
  }

  generateProducts() {
    let products: any[] = [];
    let keyCounter = 0;
    this.state.products.map(element => {
      products.push(
        <Col key = {keyCounter}>
          <Card bsPrefix='cardHover'><Card.Img variant="top" src={element.image} />
            <Card.Body>
              <Card.Title>{element.name}</Card.Title>
              <Card.Text>{element.description}</Card.Text>
              <Button variant="primary" onClick={() => this.onAddToCart(element.name)} >Add to cart</Button>
            </Card.Body>
          </Card>
        </Col>
        );
        keyCounter++;
    })
    return products;
  }

  render() {
    return (
      <>
        <Layout>
          <Container>
            <Row className='  '>
              <Col md className=' m-3 '>
                <Card bsPrefix='cardHover'>
                  <Card.Body>
                    <Card.Title> {this.props.category} Fruits</Card.Title>
                    <Card.Text> An excellent choice when you are in need of some {this.props.category} fruits. Here is our selection of the
                      finest {this.props.category} fruits man can buy.
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <Row>
              {this.generateProducts()}
            </Row>
          </Container>
        </Layout>
      </>
    );
  }

};

export default ProductPage;
