import React from 'react';
import '../../App.css'
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import lemon from '../../assets/lemon.png';
import Layout from '../../components/layout';
import { Link, Outlet } from 'react-router-dom';
import axios, { AxiosResponse } from 'axios';
import { stringify } from 'querystring';
import { isTemplateSpan } from 'typescript';
import { IProduct } from '../../../../../server/src/model/interfaces/product.interface';
import { frontendUser } from "../../model/frontendUser.class";
import loginChecker from '../../model/loginCheck';

/**
 * Class for displaying the users cart.
 * state variable Cart Maps a productname to a quantity of that product. 
 * state variable Products contains all the IProducts the user currently has in cart
 * state variable loadedCart is used to display a loading animation while the page is loading
 */
class CartPage extends React.Component<{}, { user: frontendUser, cart: Map<string, number>, products: Array<IProduct>, loadedCart: boolean, loggedIn: boolean }> {

  constructor(props: {}) { // Använd user istället för state
    super(props);
    this.state = {
      user: new frontendUser(
        "",
        "",
        "",
        "",
        "",
        0,
        "",
        new Date(),
        new Map<string, number>(),
      ),
      loadedCart: false,
      cart: new Map<string, number>(),
      products: [],
      loggedIn: true,
    };

    this.createTable = this.createTable.bind(this);
    this.buyFunction = this.buyFunction.bind(this);

    /**
     * Get cart and the associated products
     * If there is no one logged in set loggedIn variable to false.
     */
    this.componentDidMount = async () => {

      let loginCheck = new loginChecker();

      if (await loginCheck.check("user")) {
        let userRes = await axios.get<frontendUser>("http://localhost:8080/user/");
        let productRes = await axios.get<Array<IProduct>>("http://localhost:8080/product/usersProduct");
        
        this.setState({
          user: userRes.data,
          loggedIn: true,
          products: productRes.data,
          cart: new Map(Object.entries(userRes.data.cart)),
          loadedCart: true
        })

        return;
      }
      else {
        this.setState({
          loggedIn: false,
          loadedCart: true,
        })
        return;
      }
    }
  }

  /**
  * Decrease the item in cart by one
  * @param item item to remove
  * @returns updated state
  */
  async decreaseAmount(item: string) {
    const cartRes: AxiosResponse = await axios.put<Map<string, number>>("http://localhost:8080/cart/removeoneitem", { item: item });
    this.setState({
      cart: new Map(Object.entries(cartRes.data))
    })
    return;
  }

  /**
   * Increase the item in cart by one
   * @param item item to remove
   * @returns updated state
   */
  async increaseAmount(item: string) {
    const cartRes: AxiosResponse = await axios.put<Map<string, number>>("http://localhost:8080/cart/additem", { item: item, quantity: 1 })
    this.setState({
      cart: new Map(Object.entries(cartRes.data))
    })
    return;
  }

  /**
   * Removes entire row of item. i.e if you have 5 apples it removes all 5.
   * @param item item to remove
   * @returns updated state
   */
  async deleteRow(item: string) {
    const cartRes: AxiosResponse = await axios.put<Map<string, number>>("http://localhost:8080/cart/removeitem", { item: item, quantity: 1 })
    this.setState({
      cart: new Map(Object.entries(cartRes.data))
    })
    return;
  }

  totalCost(){
    let cost = 0;
    this.state.cart.forEach((value: number, key: string) => {
      cost = cost + this.state.products.find(element => element.name == key)!.price * value;
    });
    return cost;
  }
  
  /**
   * Creates the table where the cart information is displayed
   * @returns an array of HTML code with a table inside
   */
  createTable = () => {
    let table: any = []
    let outerKey = 0;
    // Outer loop to create parent
    this.state.cart.forEach((value: number, key: string) => {
      let children = []
      let innerKey = 0;
      children.push(<td key={innerKey}>{key}</td>);
      children.push(<td key={innerKey + 1}>{value} st</td>);
      children.push(<td key={innerKey + 2}>{this.state.products.find(element => element.name == key)!.price * value} kr</td>);
      children.push(<td key={innerKey + 3}>
        <Button bsPrefix="decreaseButton" onClick={() => this.decreaseAmount(key)}>-</Button>
        <Button bsPrefix="increaseButton" onClick={() => this.increaseAmount(key)}>+</Button>
        <Button bsPrefix="deleteRowButton" onClick={() => this.deleteRow(key)}>x</Button>
      </td>
      );
      table.push(<tr key={outerKey}>{children}</tr>);
      outerKey++;
    })
    return table
  }

  /**
   * Takes a user and verifies the cardinfo and the adress
   * @param user the user to verify paymentInfo on
   * @returns 
   */
  validPayment(): boolean {
    if (this.state.user.cardnr == undefined || this.state.user.expdate == undefined || this.state.user.cvc == undefined) {
      alert("No paymentinfo found, please add your card information under profile -> payment")
      return false;
    }
    //Apparently if there is no adress its not undefined or null, its an empty string with length 0. 
    if (this.state.user.adress.length == 0 || isNaN(this.state.user.postco) || this.state.user.postco == null) {
      alert("Invalid adress. Please update under profile");
      return false;
    }
    if (new Date(this.state.user.expdate).getTime() < new Date().getTime()) {
      alert("Your credit card has expired, please update your cardinformation under profile -> payment")
      return false;
    }
    return true;
  }

  /**
   * Function for buying all items in cart
   * @returns updates state
   */
  async buyFunction() {

    if (!this.validPayment()) {
      return;
    }
    axios.post<never>("http://localhost:8080/order", { totalPrice: this.totalCost(), datePlaced: new Date() });
    window.location.reload();
    return;
  }

  render() {
    if (!this.state.loadedCart) {
      return (
        <Layout>
          <Container className="h-100">
            <Row className=' flex mb-4'>
              <Col className='d-flex justify-content-center'>
                <Card bsPrefix='loadingCard'>
                  <Card.Body>
                    <Card.Title>Loading...</Card.Title>
                    <div className="lds-dual-ring"></div>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </Layout>
      );
    }
    if (!this.state.loggedIn) {
      return (
        <Layout>
          <Container className="h-100">
            <Row className=' flex mb-4'>
              <Col className='d-flex justify-content-center'>
                <Card bsPrefix='noProdCard'>
                  <Card.Body>
                    <Card.Title>Log in to see products</Card.Title>
                    <Card.Text>Whoops! It seems as you are not logged in! Log in to start shopping!</Card.Text>
                    <Link to="/LoginPage"><Card.Text>Log in </Card.Text></Link>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </Layout>
      );
    }
    if (this.state.cart.size == 0) {
      return (
        <Layout>
          <Container className="h-100">
            <Row className=' flex mb-4'>
              <Col className='d-flex justify-content-center'>
                <Card bsPrefix='noProdCard'>
                  <Card.Body>
                    <Card.Title>No products</Card.Title>
                    <Card.Text>Whoops! This is empty and sad. Add fruit to be happy!</Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </Layout>
      );
    }
    else {
      return (
        <Layout>
          <Container className="h-100">
            <table className="table text-center h-100">
              <thead>
                <tr>
                  <th scope="col">Product</th>
                  <th scope="col">quantity</th>
                  <th scope="col">price</th>
                </tr>
              </thead>
              <tbody>
                {this.createTable()}
              </tbody>
            </table>
            <div> <br></br></div>
            <Row className=' flex mb-4'>
              <Col className='d-flex justify-content-center'>
                <Card bsPrefix='buyCard'>
                  <Card.Body>
                    <Card.Title>BUY! BUY! BUY!</Card.Title>
                    <Card.Text>You are only one click and {this.totalCost()} kr away from some good good fruit</Card.Text>
                    <Button variant="primary" onClick={() => this.buyFunction()} >Buy</Button>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
        </Layout>
      );
    }
  }
};

export default CartPage;