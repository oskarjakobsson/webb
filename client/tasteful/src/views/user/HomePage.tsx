import React from 'react';
import '../../App.css'
import { Container, Row, Col, Card, Overlay } from 'react-bootstrap';
import yellow from '../../assets/lemon.png';
import red from '../../assets/berry.png';
import orange from '../../assets/orange.png';
import green from '../../assets/pear.png';
import blue from '../../assets/blueberry.png';
import Layout from '../../components/layout';
import { Link } from 'react-router-dom';
import axios, { AxiosResponse } from 'axios';
import { frontendProduct } from '../../model/frontendProduct.class';
class HomePage extends React.Component<{}, { product: frontendProduct[]}> {

  constructor(props: {}) { // Använd user istället för state
    super(props);

    this.state = {
      product: [],
    };

    this.componentDidMount = async () => {
      const productRes: AxiosResponse = await axios.get("http://localhost:8080/product/allProducts");
      this.setState({
        product: productRes.data,
      })
    }
  }

  listCategories() {
    let listOfCategories: any[] = [];
    let proccessedCategories: any[] = [];
    let keyCounter = 0;
    this.state.product.forEach(element => {
      //Only return category not already displayed
      if (!proccessedCategories.includes(element.category.toLocaleLowerCase())) {
        proccessedCategories.push(element.category.toLocaleLowerCase());
        listOfCategories.push(
          <Col md key={keyCounter} className='m-3'>
            <Link className='links' to={'/ProductPage/' + element.category} >
              <Card bsPrefix='cardHover' >
                <Card.Img variant="top" src={element.image} />
                <Card.Body >
                  <Card.Title>{element.category} Fruits!!</Card.Title>
                  <Card.Text>Refreshing</Card.Text>
                </Card.Body>
              </Card>
            </Link>
          </Col>
        );
        keyCounter++;
      }
    })
    return listOfCategories;
  }

  render() {
    return (
      <>
        <Layout>
          <Container>
            <Row >
              {this.listCategories()}
            </Row>
          </Container>
        </Layout>
      </>
    );
  }
};

export default HomePage;
