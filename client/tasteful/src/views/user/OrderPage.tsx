import React, { Children } from 'react';
import '../../App.css'
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import lemon from '../../assets/lemon.png';
import Layout from '../../components/layout';
import { Link, Outlet } from 'react-router-dom';
import axios, { AxiosResponse } from 'axios';


//import Map from "./MapComponent";
import { stringify } from 'querystring';
import { isTemplateSpan } from 'typescript';
import { IProduct } from '../../../../../server/src/model/interfaces/product.interface';
import { resourceUsage } from 'process';
import { frontendOrder } from '../../model/frontendOrder.class';
import loginChecker from '../../model/loginCheck';
/**
 * Class view for displaying the users Orders
 * State variable loadedPage: true = page has loaded, false = page has not loaded yet
 * State variable loggedIn: true = user is logged in, false = user is not logged in
 * State variable orders: List containing all the orders the user has created
 * State variable products: Array containing all products that the users Order contains.
 */
class OrderPage extends React.Component<{}, { loadedPage: boolean, loggedIn: boolean, orders: frontendOrder[], products: Array<IProduct> }> {

    constructor(props: {}) { // Använd user istället för state
        super(props);
        this.state = {
            loadedPage: false,
            loggedIn: false,
            orders: [],
            products: []
        };

        this.componentDidMount = async () => {
            //Not really necessary since you can only navigate to OrderPage from profilepage and you can only navigate there if you are logged in
            //But if a user does /OrderPage manually they will atleast be greeted by something now.
            let loginCheck = new loginChecker();
            if (await loginCheck.check("user")) {
                const allOrderRes = await axios.get("http://localhost:8080/order");
                let orders: frontendOrder[] = (allOrderRes.data as frontendOrder[]);
                let productRes = await axios.get<Array<IProduct>>("http://localhost:8080/product/orderedproducts");
                orders = orders.reverse();
                this.setState({
                    loggedIn: true,
                    loadedPage: true,
                    orders: orders,
                    products: productRes.data,
                })
                return;
            }
            else {
                this.setState({
                    loggedIn: false,
                    loadedPage: true,
                })
                return;
            }

        }
    }

    createChildren(value: number, key: string) {
        let items: any = [];
        let index = 0;
        items.push(<td key={index} className='orderTd'>{key}</td>);
        items.push(<td key={index + 1} className='orderTd'>{value} st</td>);
        items.push(<td key={index + 2} className='orderTd'> {this.state.products.find(element => element.name == key)!.price * value} kr</td>);
        return items;
    }
    createTable(children: any[]) {
        let table: any = [];
        table.push(
            <table key={0} className="orderTable">
                <thead>
                    <tr>
                        <th className='orderTh'>Item</th>
                        <th className='orderTh'>Quantity</th>
                        <th className='orderTh'>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {children}
                </tbody>
            </table>
        );
        return table;
    }
    createCard(value: frontendOrder, table: any[]) {
        let card: any = [];
        card.push(
            <Card key={0} bsPrefix='productCard'>
                <Card.Body>
                    <Card.Title>OrderId: {value.orderID} </Card.Title>
                    <Card.Text>Status: {value.delieredString}</Card.Text>
                    <Card.Text>Ordered on : {new Date(value.datePlaced).toString().substring(0, 21)}</Card.Text>
                    <Card.Text>Order adress: {value.adress + ", " + value.postco.slice(0, 3) + " " + value.postco.slice(3, 5) + ", " + value.city}</Card.Text>
                    <Card.Text>totalCost: {value.totalPrice} kr</Card.Text>
                    <Card.Text bsPrefix="orderHeadline">Ordered items</Card.Text>
                    {table}
                </Card.Body>
            </Card>);
        return card;
    }

    createOrderView() {
        let listOfCards: any = [];
        let outerIndex = 0;
        this.state.orders.forEach((value) => {
            let items: any = [];
            let table: any = [];
            let children: any = [];
            let localMap = new Map<string, number>();
            let innerIndex = 0;
            localMap = new Map(Object.entries(value.items));
            localMap.forEach((value: number, key: string) => {
                items = this.createChildren(value, key);
                children.push(<tr key={innerIndex}>{items}</tr>);
                innerIndex++;
            });
            table = this.createTable(children);
            let card: any = [];
            card = this.createCard(value, table);
            listOfCards.push(
                <Row key={outerIndex} className=' flex mb-4'>
                    <Col className='d-flex justify-content-center'>
                        {card}
                    </Col>
                </Row>
            );
            outerIndex++;
        });
        return listOfCards;
    }

    render() {
        if (!this.state.loadedPage) {
            return (
                <Layout>
                    <Container className="h-100">
                        <Row className=' flex mb-4'>
                            <Col className='d-flex justify-content-center'>
                                <Card bsPrefix='loadingCard'>
                                    <Card.Body>
                                        <Card.Title>Loading...</Card.Title>
                                        <div className="lds-dual-ring"></div>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </Layout>
            );
        }
        if (!this.state.loggedIn) {
            return (
                <Layout>
                    <Container className="h-100">
                        <Row className=' flex mb-4'>
                            <Col className='d-flex justify-content-center'>
                                <Card bsPrefix='loadingCard'>
                                    <Card.Body>
                                        <Card.Title>Create an account to see your orders!</Card.Title>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </Layout>
            );
        }
        if (this.state.orders.length == 0) {
            return (
                <Layout>
                    <Container className="h-100">
                        <Row className=' flex mb-4'>
                            <Col className='d-flex justify-content-center'>
                                <Card bsPrefix='noProdCard'>
                                    <Card.Body>
                                        <Card.Title>No Orders!</Card.Title>
                                        <Card.Text>Whoops! This is empty and sad. Add fruit to be happy!</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </Layout>
            );
        }
        else {
            return (
                <Layout>
                    <Container className="h-100">
                        {this.createOrderView()}
                    </Container>
                </Layout>
            );
        }

    }
};

export default OrderPage;
