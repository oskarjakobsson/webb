import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import lemon from '../../lemon.png';
import { frontendUser } from "../../model/frontendUser.class";
import { Link, Navigate } from 'react-router-dom';
axios.defaults.withCredentials = true;
/**
 * Class view for displaying the login page.
 * State variable user for the user object
 * State variable redirect to redirect the user to the profilepage if there was a successfull
 */
class LoginPage extends React.Component<{}, {user: frontendUser, redirect: boolean }> {

  constructor(props: {}) { // Använd user istället för state
    super(props);

    this.state = {
      user: new frontendUser("",
        "",
        "",
        "",
        "",
        0,
        "",
        new Date(0),
        new Map<string, number>(),
        0,
        "",
        new Date(0),
        ""
      ),
      redirect: false,
    };

    this.onLogin = this.onLogin.bind(this);

    this.componentDidMount = async () => {

    }
  }
  /**
   * onLogin checks if the inputted information is correct and in the case of an unsuccessful attempt it alerts the user that the input was incorrect
   * On a successful attempt redirects the user to their profilepage
   * @returns sets the state depending on the outcome of login attempt
   */
  private async onLogin() {
    let success: boolean = true;
    await axios.put<never>("http://localhost:8080/user/login", { user: this.state.user }).catch(err => {
      if (err.response) {
        alert("Invalid password or email!");
        success = false;
        return;
      }
    });
    
    if(!success) return;
      
    this.setState({ redirect: true });

  }

  render() {
    if (this.state.redirect) {
      return <Navigate to="/ProfilePage" />;
    }
    return (
      <Layout>
        <Container >
          <Row className="justify-content-md-center">
            <Col xs lg="6">
              <Card >
                <Card.Body className="flex">
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicName">
                      <Form.Label className='small mb-1'>Email</Form.Label>
                      <Form.Control type="text" placeholder="Enter Email" value={this.state.user.email} onChange={e => this.setState(prevState => ({
                        user: {                   // object that we want to update
                          ...prevState.user,    // keep all other key-value pairs
                          email: e.target.value      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label className='small mb-1'>Password</Form.Label>
                      <Form.Control type="password" placeholder="Enter password" value={this.state.user.password} onChange={e => this.setState(prevState => ({
                        user: {                   // object that we want to update
                          ...prevState.user,    // keep all other key-value pairs
                          password: e.target.value.toString()      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Button variant="primary" onClick={this.onLogin} >
                      Login
                    </Button>
                  </Form><br></br>
                  <Col className="d-flex justify-content-between">
                  <div> Dont have an account? <Link to="/RegisterPage">Register here! </Link></div>
                 <Link to="/AdminLogin">Login as admin! </Link>
                  </Col>
                  
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </Layout>
    );
  }
};

export default LoginPage;