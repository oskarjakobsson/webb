import React from 'react';
import axios, { AxiosResponse } from 'axios';
import '../../App.css'
import { Container, Form, Button, Navbar, Nav, Col, Row, Card } from 'react-bootstrap';
import Layout from '../../components/layout';
import lemon from '../../lemon.png';
import { frontendUser } from "../../model/frontendUser.class";
import { Link, Outlet, Navigate } from 'react-router-dom';

class RegisterPage extends React.Component<{}, { user: frontendUser, redirect: boolean }> {

  constructor(props: {}) { // Använd user istället för state
    super(props);

    this.state = {

      user: new frontendUser(
        "",
        "",
        "",
        "",
        "",
        0,
        "",
        new Date(0),
        new Map<string, number>(),
        0,
        "",
        new Date(0),
        ""
      ),
      redirect: false,
    };


    this.onAddUser = this.onAddUser.bind(this);

    this.componentDidMount = async () => {
      this.setState(prevState => ({
        user: {                   // object that we want to update
          ...prevState.user,    // keep all other key-value pairs
          birthday: new Date('December 17, 2000 03:24:00'),    // update the value of specific key
        }
      }))

    }
  }

  private async onAddUser() {
    let uglyBoolean: boolean = false;
    await axios.post<never>("http://localhost:8080/user", { user: this.state.user }).catch(function (error) {
      if (error.response) {
        alert(error.response.data);
        console.log(error.response.data);
        uglyBoolean = true;
      }
    });
    if (!uglyBoolean) {
      this.setState({
        redirect: true,
      });
    }
  }

  confirmNumber(input: string) {
    if (input.length == 0) {
      this.setState(prevState => ({
        user: {                   // object that we want to update
          ...prevState.user,    // keep all other key-value pairs
          phone: ""      // update the value of specific key
        }
      }))
    }

    for (var i = 1; i < input.length; i++) {
      if (isNaN(parseInt(input[i]))) {
        return;
      }
    }
    if (isNaN(parseInt(input[0])) && input[0] != "+") {
      return;
    }
    this.setState(prevState => ({
      user: {                   // object that we want to update
        ...prevState.user,    // keep all other key-value pairs
        phone: input      // update the value of specific key
      }
    }))
  }

  render() {
    if (this.state.redirect) {
      return <Navigate to="/ProfilePage" />;
    }
    return (
      <Layout>
        <Container >
          <Row >
            <Col>
              <Card>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicName">
                      <Form.Label className='small mb-1'>Name</Form.Label>
                      <Form.Control type="text" placeholder="Enter name" value={this.state.user.name} onChange={e => this.setState(prevState => ({
                        user: {                   // object that we want to update
                          ...prevState.user,    // keep all other key-value pairs
                          name: e.target.value      // update the value of specific key
                        }
                      }))} />
                    </Form.Group>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                          <Form.Label className='small mb-1'>Email address</Form.Label>
                          <Form.Control type="email" placeholder="Enter email" value={this.state.user.email} onChange={e => this.setState(prevState => ({
                            user: {                   // object that we want to update
                              ...prevState.user,    // keep all other key-value pairs
                              email: e.target.value      // update the value of specific key
                            }
                          }))} />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                          <Form.Label className='small mb-1'>Password</Form.Label>
                          <Form.Control type="password" placeholder="Enter password" value={this.state.user.password} onChange={e => this.setState(prevState => ({
                            user: {                   // object that we want to update
                              ...prevState.user,    // keep all other key-value pairs
                              password: e.target.value.toString()      // update the value of specific key
                            }
                          }))} />
                        </Form.Group>
                      </Col>

                    </Row>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicCity">
                          <Form.Label className='small mb-1'>City</Form.Label>
                          <Form.Control type="city" placeholder="Enter City" value={this.state.user.city} onChange={e => this.setState(prevState => ({
                            user: {                   // object that we want to update
                              ...prevState.user,    // keep all other key-value pairs
                              city: e.target.value      // update the value of specific key
                            }
                          }))} />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicAddress">
                          <Form.Label className='small mb-1'>Address</Form.Label>
                          <Form.Control type="address" placeholder="Enter adress" value={this.state.user.adress} onChange={e => this.setState(prevState => ({
                            user: {                   // object that we want to update
                              ...prevState.user,    // keep all other key-value pairs
                              adress: e.target.value      // update the value of specific key
                            }
                          }))} />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicPostalcode">
                          <Form.Label className='small mb-1'>Postal CO</Form.Label>
                          <Form.Control type="postalcode" placeholder="Enter postco" value={this.state.user.postco} onChange={e => this.setState(prevState => ({
                            user: {                   // object that we want to update
                              ...prevState.user,    // keep all other key-value pairs
                              postco: parseInt(e.target.value)      // update the value of specific key
                            }
                          }))} />
                        </Form.Group>
                      </Col>

                    </Row>

                    <Row>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBasicPhone">
                          <Form.Label className='small mb-1'>Phone</Form.Label>
                          <Form.Control type="tel" placeholder="Enter phonenumber" value={this.state.user.phone} onChange={e => {
                            //Dumt sätt för att kolla att inga siffror är med
                            //Behövdes för att kunna skriva nollor och för att slipa NaN
                            this.confirmNumber(e.target.value);
                          }} />
                        </Form.Group>
                      </Col>
                      <Col>
                        <Form.Group className="mb-3" controlId="formBirthday">
                          <Form.Label className='small mb-1'> Birthday</Form.Label>
                          <Form.Control type="date" value={this.state.user.birthday.toISOString().split('T')[0]} onChange={e => this.setState(prevState => ({
                            user: {                   // object that we want to update
                              ...prevState.user,    // keep all other key-value pairs
                              birthday: new Date(e.target.value)      // update the value of specific key
                            }
                          }))} />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Button variant="primary" onClick={this.onAddUser} >
                      Register
                    </Button>
                  </Form>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </Layout>
    );
  }
};

export default RegisterPage;