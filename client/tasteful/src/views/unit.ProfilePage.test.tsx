import { getByTestId } from "@testing-library/react";
import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import ProfilePage from "./user/HomePage";
/*
describe("<ProfilePage />", () => {
      test("should display a blank login form, with remember me checked by default", async () => {
        
       const loginForm = getByTestId( "form", "formBasicName");

  expect(loginForm).toHaveFormValues({
    username: "",
    password: "",
    remember: true
 });
      });
  });

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("Form");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders user data", async () => {
  const fakeUser = {
    name: "Joni Baez",
    email: "j@m.se",
    address: "123, Charming Avenue"
  };
  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(fakeUser)
    })
  );

  // Use the asynchronous version of act to apply resolved promises
  await act(async () => {
    render(<ProfilePage id="123" />, container);
  });

  expect(container.querySelector("From.Control").textContent).toBe(fakeUser.name);
  expect(container.querySelector("strong").textContent).toBe(fakeUser.age);
  expect(container.textContent).toContain(fakeUser.address);

  // remove the mock to ensure tests are completely isolated
  global.fetch.mockRestore();
});

*/