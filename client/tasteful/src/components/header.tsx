import axios, { AxiosResponse } from 'axios';
import * as React from 'react';
import { Button, Container, Form, Image, Navbar, Row, Col } from 'react-bootstrap';
import { PersonCircle, Bag } from 'react-bootstrap-icons';
import { Link, Outlet } from 'react-router-dom';
axios.defaults.withCredentials = true;



interface IHeaderProps {

}

class header extends React.Component<{}, { navigateLink: string }> {

  constructor(props: {}) { // Använd user istället för state
    super(props);

    this.state = {
      navigateLink: "",
    };


    this.componentDidMount = async () => {
      try {
        const res: AxiosResponse<String> = await axios.get("http://localhost:8080/user/checkSession");
        if (res.data == "no session")
          this.setState({
            navigateLink: "/LoginPage",
          })
        else if (res.data == "session exists") {
          this.setState({
            navigateLink: "/ProfilePage",
          })
        }
      } catch (error: any) {
        console.log(error.message);
      }
    }
  }

  render() {
    return (
      <Container className='flex'>
        <Row >
          <Col className='d-flex justify-content-center'> <Link to="/" ><Image src='../Logo.png' alt='tasteful' height={250} /> </Link>  </Col>
        </Row>
        <Row className='p-3'>
          <Col></Col>
          <Col xs={6} >
            <Form className='d-flex justify-content-end '>
              <Form.Control type='search' placeholder='search' aria-label='search' className='me-2' />
              <Button className="btn btn-outline-success my-2 my-sm-0" color='#5D7B3D' type="submit">Search</Button>
            </Form>
          </Col>
          <Col className='d-flex justify-content-end'> <Link to={this.state.navigateLink}> <PersonCircle className='ml-4' size={30} color='#5D7B3D' /> </Link>  <Link to="/CartPage"> <Bag className='ml-4' size={30} color='#5D7B3D' /> </Link></Col>

        </Row>
        <Outlet />
      </Container>
    );
  }
}





/*
 const Header: React.FunctionComponent<IHeaderProps> = (props) => {


  async function checkIfLoggedin (){
    console.log("hallå");
    const res: AxiosResponse<User[]> = await axios.get<User[]>("http://localhost:8080/user");
    if(res.data != undefined){
      console.log("inside if-statement");
      props.navigateLink = "/LoginPage"
    }

    props.navigateLink = "/ProfilePage"
  }
  
  return (

    

<Container className='flex'>
  <Row >
    <Col className='d-flex justify-content-center'> <Link to="/" ><Image src='./Logo.png' alt='tasteful'  height={250}/> </Link>  </Col>
  </Row>
  <Row  className='p-3'>
    <Col></Col>
    <Col xs={6} >
      <Form className='d-flex justify-content-end '>
              <Form.Control type='search'  placeholder='search' aria-label='search' className='me-2' />
              <Button className="btn btn-outline-success my-2 my-sm-0" color='#5D7B3D'  type="submit">Search</Button>
            </Form>
    </Col>
    <Col className='d-flex justify-content-end'> <Link to={props.navigateLink}> <PersonCircle className='ml-4' size={30} color='#5D7B3D' /> </Link>  <Link to="/CartPage"> <Bag className='ml-4' size={30} color='#5D7B3D' /> </Link></Col>
    
  </Row>
  <Outlet/>
</Container>

     

    


  );
};
*/
export default header;
