import React from 'react';
import '../App.css'
import Header from './header';
import Footer from './footer';
import { Container, Row, Col, Card, Button, Navbar, Nav } from 'react-bootstrap';
import AdminHeader from './adminHeader';


const AdminLayout: React.FC = ({ children }) => {
    return (
        <Container className="d-flex flex-column min-vh-100 mb-auto" bsPrefix="bodyContainer">
            <Container className="d-flex flex-column h-100 mb-auto" bsPrefix="contentContainer">

                <AdminHeader />


                <main >
                    <Container>
                        <Navbar className="d-flex" >

                            <Nav className="mr-auto">
                                <Nav.Link href="/AdminPage"> <p> Profile</p> </Nav.Link>
                                <Nav.Link href="/AdminAddProduct"> <p> Add product</p> </Nav.Link>
                                <Nav.Link href="/AdminUpdateProduct"> <p> Update product</p> </Nav.Link>
                                <Nav.Link href="/AdminManageOrders"> <p> Manage orders</p> </Nav.Link>
                                <Nav.Link href="/AdminManageProfiles"> <p> Manage profiles</p> </Nav.Link>

                            </Nav>
                        </Navbar>

                        {children}
                    </Container></main>

            </Container>
            <Footer />
        </Container>
    );
};

export default AdminLayout;