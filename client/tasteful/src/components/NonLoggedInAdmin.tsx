import React from 'react';
import '../App.css'
import Header from './header';
import Footer from './footer';
import { Container, Row, Col, Card, Button, Navbar, Nav } from 'react-bootstrap';
import AdminHeader from './adminHeader';
import AdminLayout from './adminLayout'
import { Link, Navigate } from 'react-router-dom';



const NonLoggedInAdmin: React.FC = ({ children }) => {
    return (
        <AdminLayout>
            <Container >
                <Row className="justify-content-md-center">
                    <Col xs lg="6">
                        <Card>
                            <Card.Body>
                                <Card.Title> You have to be logged in to access this information!</Card.Title>  
                                <Card.Text> <Link to="/AdminLogin"> Login </Link> </Card.Text>                             
                                Dont have an account? <Link to="/AdminRegister">Request admin account here! </Link>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </AdminLayout>
    );
};

export default NonLoggedInAdmin;
