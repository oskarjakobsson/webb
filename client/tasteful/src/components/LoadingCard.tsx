import React from 'react';
import '../App.css'
import Header from './header';
import Footer from './footer';
import { Container, Row, Col, Card, Button, Navbar, Nav } from 'react-bootstrap';


const LoadingCard: React.FC = ({ children }) => {
    return (
        <Row className=' flex mb-4'>
            <Col className='d-flex justify-content-center'>
                <Card bsPrefix='loadingCard'>
                    <Card.Body>
                        <Card.Title>Loading...</Card.Title>
                        <div className="lds-dual-ring"></div>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
};

export default LoadingCard;

