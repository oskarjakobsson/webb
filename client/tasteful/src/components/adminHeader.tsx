import axios, { AxiosResponse } from 'axios';
import * as React from 'react';
import { Button, Container, Form, Image, Navbar, Row, Col } from 'react-bootstrap';
import { PersonCircle, Bag } from 'react-bootstrap-icons';
import { Link, Outlet } from 'react-router-dom';
axios.defaults.withCredentials = true;



interface IAdminHeaderProps {

}

class AdminHeader extends React.Component<{}, { navigateLink: string }> {

  constructor(props: {}) { // Använd user istället för state
    super(props);

    this.state = {
      navigateLink: "",
    };


    this.componentDidMount = async () => {

    }
  }

  render() {
    return (
      <Container className='flex'>
        <Row >
          <Col className='d-flex justify-content-center'>
            <Link to="/AdminPage" >
              <Image src='../adminlogo.png' alt='tasteful' height={250} />
            </Link>
          </Col>
        </Row>
        <Row className='p-3'>
          <Col>
          </Col>
        </Row>
        <Outlet />
      </Container>
    );
  }
}
export default AdminHeader;
