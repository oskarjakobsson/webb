import * as React from 'react';
import { Button, Container, Form } from 'react-bootstrap';

interface IRegisterProps {
}

const Register: React.FunctionComponent<IRegisterProps> = (props) => {
  return (
      <Container>
           <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
          </Form>
      </Container>
  );
};

export default Register;
