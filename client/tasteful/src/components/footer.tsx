import * as React from 'react';
import { Container, Col, Row , Button} from 'react-bootstrap';
import {Instagram, Facebook} from 'react-bootstrap-icons';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

interface IFooterProps {
}

const Footer: React.FunctionComponent<IFooterProps> = (props) => {
  return (
     
    <MDBFooter className="bg-dark text-center text-white pt-4 mt-5" >
    <div className=" text-center ">
      <ul className="list-unstyled list-inline">
        <li className="list-inline-item">
          <a className="btn-floating btn-sm btn-fb mx-1">
           <Facebook color='pink' fill="currentColor" size={22}/>
          </a>
        </li>
        <li className="list-inline-item">
          <a className="btn-floating btn-sm btn-tw mx-1">
            <Instagram fill="currentColor" size={22}/>
          </a>
        </li>
        
      </ul>
    </div>
    
    <a className="mail" href="mailto: TASTEFUL@fruitmail.com"> TASTEFUL@fruitmail.com</a>
    <div className="footer-copyright text-center py-3 mt-3">
      <MDBContainer fluid >
        Tasteful co.
      </MDBContainer>
    </div>
  </MDBFooter>
  
         
  );
};

export default Footer;





    