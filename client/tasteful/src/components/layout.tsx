import React from 'react';
import '../App.css'
import Header from './header';
import Footer from './footer';
import { Container, Row, Col, Card, Button, Navbar, Nav} from 'react-bootstrap';


const Layout: React.FC = ({ children }) => {
  return (
    <Container className="d-flex flex-column min-vh-100 mb-auto" bsPrefix="bodyContainer">
      <Container className="d-flex flex-column h-100 mb-auto" bsPrefix="contentContainer">

        <Header />

       

        <main >{children}</main>

      </Container>
      <Footer />
    </Container>
  );
};

export default Layout;
