export class frontendProduct {
    name: string;
    category: string;
    price: number;
    quantity: number;
    description: string;
    image: string;
    constructor(name: string, category: string, price: number, quantity: number, description: string, image: string) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
        this.image = image;
    }
}