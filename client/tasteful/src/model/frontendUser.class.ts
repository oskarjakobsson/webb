import mongoose from "mongoose";

export class frontendUser {

    name: string;
    email: string;
    password: string;
    city: string;
    adress: string;
    postco: number;
    phone: string;
    birthday: Date;
    cardnr?: string;
    expdate?: Date;
    cvc?: string;
    cart: Map<string, number>;
    totalCost?: number;
    
    constructor(name: string,
      email: string,
      password: string,
      city: string,
      adress: string,
      postco: number,
      phone: string,
      birthday: Date,
      cart: Map<string, number>,
      totalCost?: number,
      cardnr?: string,
      expdate?: Date,
      cvc?: string) {
        //super(User);
  
      this.name = name;
      this.email = email;
      this.password = password;
      this.city = city;
      this.adress = adress;
      this.postco = postco;
      this.phone = phone;
      this.birthday = birthday;
      this.cardnr = cardnr;
      this.expdate = expdate;
      this.cvc = cvc;
      this.cart = cart;
      this.totalCost = totalCost;
    }
  }