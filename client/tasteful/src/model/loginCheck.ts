import axios from "axios";

export class loginChecker {

    constructor(){ }
    
    async check(typeOfUser: String): Promise<boolean> {

        let url: string = "http://localhost:8080/" + typeOfUser + "/checkSession";
        const res = await axios.get<never>(url);
            if (res.data == "session exists") {
                return true;
            }
            else if (res.data == "no session") {
                return false;
            }
        return false;
    }
}

export default loginChecker;