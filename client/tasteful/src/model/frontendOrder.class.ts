import mongoose from "mongoose";

export class frontendOrder  {
    
    user: string;
    orderID: string;
    items: Map<string, number>;
    totalPrice: Number;
    city: string;
    adress: string;
    postco: string;
    phone: string;
    datePlaced: Date;
    delieredString: string;
    
    constructor( user: string,
        orderID: string,
        items: Map<string, number>,
        totalPrice: Number,
        city: string,
        adress: string,
        postco: string,
        phone: string,
        datePlaced: Date,
        deliveredString: string){

        this.user = user;
        this.orderID = orderID;
        this.items = items;
        this.totalPrice = totalPrice;
        this.city = city;
        this.adress =  adress;
        this.postco = postco;
        this.phone = phone;
        this.datePlaced = datePlaced;
        this.delieredString = deliveredString;
    }
}