# README Group 03 #


### File organisation ###

* Backend logic in server directory
* Frontend logic in client/tasteful dierctory

### How to run ###

* cd into /server and run npm run dev to start the server environment
* cd into /client/tasteful and type npm start to run the frontend environment
* run npm t in the server directory to run tests.
* NOTE! If there is an issue with connecting to the database please email: oskjak@student.chalmers.se or annmanf@student.chalmers.se because we might have to invite you into the mongoDB deployment. (Not sure)

### Commits and version control ###

* We are not git-experts and we ran into some problems during the last three weeks so the structure might look a bit chaotic. We apologize for this! However in bitbucket there is a nice field called "commits" in the left navigation bar, where we have stated what each commit contained and who worked on it. We hope this is good enough! 

### Project report ###

* The project report is handed in in canvas under assignments.


### Who do I talk to? ###

* oskjak@student.chalmers.se, annmanf@student.chalmers.se, axetorn@student.chalmers.se